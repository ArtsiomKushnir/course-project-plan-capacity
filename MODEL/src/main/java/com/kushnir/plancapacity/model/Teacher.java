package com.kushnir.plancapacity.model;

/**
 * Teacher class
 */
public class Teacher {

    private Integer id;
    private String name;
    private Integer department;
    private String name_department;

    public Teacher() {
    }

    public Teacher(String name, Integer department) {
        this.name = name;
        this.department = department;
    }

    public Teacher(Integer id, String name, Integer department) {
        this.id = id;
        this.name = name;
        this.department = department;
    }

    public Teacher(Integer id, String name, Integer department, String name_department) {
        this.id = id;
        this.name = name;
        this.department = department;
        this.name_department = name_department;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDepartment() {
        return department;
    }

    public void setDepartment(Integer department) {
        this.department = department;
    }

    public String getName_department() {
        return name_department;
    }

    public void setName_department(String name_department) {
        this.name_department = name_department;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Teacher teacher = (Teacher) o;

        if (id != null ? !id.equals(teacher.id) : teacher.id != null) return false;
        if (name != null ? !name.equals(teacher.name) : teacher.name != null) return false;
        if (department != null ? !department.equals(teacher.department) : teacher.department != null) return false;
        return name_department != null ? name_department.equals(teacher.name_department) : teacher.name_department == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (department != null ? department.hashCode() : 0);
        result = 31 * result + (name_department != null ? name_department.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", department=" + department +
                ", name_department='" + name_department + '\'' +
                '}';
    }
}
