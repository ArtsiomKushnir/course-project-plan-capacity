package com.kushnir.plancapacity.model;

/**
 * Specialty class
 */
public class Specialty {

    private Integer id;
    private String name;
    private String code;
    private Integer id_faculty;
    private String name_faculty;

    public Specialty() {
    }

    public Specialty(String name, String code, Integer id_faculty) {
        this.name = name;
        this.code = code;
        this.id_faculty = id_faculty;
    }

    public Specialty(Integer id, String name, String code, Integer id_faculty) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.id_faculty = id_faculty;
    }

    public Specialty(Integer id, String name, String code, Integer id_faculty, String name_faculty) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.id_faculty = id_faculty;
        this.name_faculty = name_faculty;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getId_faculty() {
        return id_faculty;
    }

    public void setId_faculty(Integer id_faculty) {
        this.id_faculty = id_faculty;
    }

    public String getName_faculty() {
        return name_faculty;
    }

    public void setName_faculty(String name_faculty) {
        this.name_faculty = name_faculty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Specialty specialty = (Specialty) o;

        if (id != null ? !id.equals(specialty.id) : specialty.id != null) return false;
        if (name != null ? !name.equals(specialty.name) : specialty.name != null) return false;
        if (code != null ? !code.equals(specialty.code) : specialty.code != null) return false;
        if (id_faculty != null ? !id_faculty.equals(specialty.id_faculty) : specialty.id_faculty != null) return false;
        return name_faculty != null ? name_faculty.equals(specialty.name_faculty) : specialty.name_faculty == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (id_faculty != null ? id_faculty.hashCode() : 0);
        result = 31 * result + (name_faculty != null ? name_faculty.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Specialty{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", id_faculty=" + id_faculty +
                ", name_faculty='" + name_faculty + '\'' +
                '}';
    }
}
