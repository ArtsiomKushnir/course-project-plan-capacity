package com.kushnir.plancapacity.model;

/**
 * Department class
 */
public class Department {

    private Integer id;
    private String name;
    private Integer id_faculty;
    private String faculty_name;

    public Department() {
    }

    public Department(String name, Integer id_faculty) {
        this.name = name;
        this.id_faculty = id_faculty;
    }

    public Department(Integer id, String name, Integer id_faculty) {
        this.id = id;
        this.name = name;
        this.id_faculty = id_faculty;
    }

    public Department(Integer id, String name, Integer id_faculty, String faculty_name) {
        this.id = id;
        this.name = name;
        this.id_faculty = id_faculty;
        this.faculty_name = faculty_name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId_faculty() {
        return id_faculty;
    }

    public void setId_faculty(Integer id_faculty) {
        this.id_faculty = id_faculty;
    }

    public String getFaculty_name() {
        return faculty_name;
    }

    public void setFaculty_name(String faculty_name) {
        this.faculty_name = faculty_name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Department that = (Department) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (id_faculty != null ? !id_faculty.equals(that.id_faculty) : that.id_faculty != null) return false;
        return faculty_name != null ? faculty_name.equals(that.faculty_name) : that.faculty_name == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (id_faculty != null ? id_faculty.hashCode() : 0);
        result = 31 * result + (faculty_name != null ? faculty_name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", id_faculty=" + id_faculty +
                ", faculty_name='" + faculty_name + '\'' +
                '}';
    }
}
