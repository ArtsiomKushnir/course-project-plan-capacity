package com.kushnir.plancapacity.model;

/**
 * Capacity
 */
public class Capacity {

    private Integer id_capacity;
    private Boolean semestr;
    private String academic_year;
    private Integer id_speciality;
    private Integer id_teacher;
    private Integer id_discipline;
    private Integer id_type;
    private Double count_hours;

    public Capacity() {}

    public Capacity(Boolean semestr,
                    String academic_year,
                    Integer id_speciality,
                    Integer id_teacher,
                    Integer id_discipline,
                    Integer id_type,
                    Double count_hours) {

        this.semestr = semestr;
        this.academic_year = academic_year;
        this.id_speciality = id_speciality;
        this.id_teacher = id_teacher;
        this.id_discipline = id_discipline;
        this.id_type = id_type;
        this.count_hours = count_hours;
    }

    public Capacity(Integer id_capacity,
                    Boolean semestr,
                    String academic_year,
                    Integer id_speciality,
                    Integer id_teacher,
                    Integer id_discipline,
                    Integer id_type,
                    Double count_hours) {
        this.id_capacity = id_capacity;
        this.semestr = semestr;
        this.academic_year = academic_year;
        this.id_speciality = id_speciality;
        this.id_teacher = id_teacher;
        this.id_discipline = id_discipline;
        this.id_type = id_type;
        this.count_hours = count_hours;
    }

    public Integer getId_capacity() {
        return id_capacity;
    }

    public void setId_capacity(Integer id_capacity) {
        this.id_capacity = id_capacity;
    }

    public Boolean getSemestr() {
        return semestr;
    }

    public void setSemestr(Boolean semestr) {
        this.semestr = semestr;
    }

    public String getAcademic_year() {
        return academic_year;
    }

    public void setAcademic_year(String academic_year) {
        this.academic_year = academic_year;
    }

    public Integer getId_speciality() {
        return id_speciality;
    }

    public void setId_speciality(Integer id_speciality) {
        this.id_speciality = id_speciality;
    }

    public Integer getId_teacher() {
        return id_teacher;
    }

    public void setId_teacher(Integer id_teacher) {
        this.id_teacher = id_teacher;
    }

    public Integer getId_discipline() {
        return id_discipline;
    }

    public void setId_discipline(Integer id_discipline) {
        this.id_discipline = id_discipline;
    }

    public Integer getId_type() {
        return id_type;
    }

    public void setId_type(Integer id_type) {
        this.id_type = id_type;
    }

    public Double getCount_hours() {
        return count_hours;
    }

    public void setCount_hours(Double count_hours) {
        this.count_hours = count_hours;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Capacity capacity = (Capacity) o;

        if (id_capacity != null ? !id_capacity.equals(capacity.id_capacity) : capacity.id_capacity != null)
            return false;
        if (semestr != capacity.semestr) return false;
        if (academic_year != null ? !academic_year.equals(capacity.academic_year) : capacity.academic_year != null)
            return false;
        if (id_speciality != null ? !id_speciality.equals(capacity.id_speciality) : capacity.id_speciality != null)
            return false;
        if (id_teacher != null ? !id_teacher.equals(capacity.id_teacher) : capacity.id_teacher != null) return false;
        if (id_discipline != null ? !id_discipline.equals(capacity.id_discipline) : capacity.id_discipline != null)
            return false;
        if (id_type != null ? !id_type.equals(capacity.id_type) : capacity.id_type != null) return false;
        return count_hours != null ? count_hours.equals(capacity.count_hours) : capacity.count_hours == null;
    }

    @Override
    public int hashCode() {
        int result = id_capacity != null ? id_capacity.hashCode() : 0;
        result = 31 * result + (semestr != null ? semestr.hashCode() : 0);
        result = 31 * result + (academic_year != null ? academic_year.hashCode() : 0);
        result = 31 * result + (id_speciality != null ? id_speciality.hashCode() : 0);
        result = 31 * result + (id_teacher != null ? id_teacher.hashCode() : 0);
        result = 31 * result + (id_discipline != null ? id_discipline.hashCode() : 0);
        result = 31 * result + (id_type != null ? id_type.hashCode() : 0);
        result = 31 * result + (count_hours != null ? count_hours.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Capacity{" +
                "id_capacity=" + id_capacity +
                ", semestr=" + semestr +
                ", academic_year='" + academic_year + '\'' +
                ", id_speciality=" + id_speciality +
                ", id_teacher=" + id_teacher +
                ", id_discipline=" + id_discipline +
                ", id_type=" + id_type +
                ", count_hours=" + count_hours +
                '}';
    }
}
