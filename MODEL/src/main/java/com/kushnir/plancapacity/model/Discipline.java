package com.kushnir.plancapacity.model;

/**
 * Discipline class
 */
public class Discipline {

    private Integer id;
    private String name;
    private Integer idDepartment;
    private String name_department;

    public Discipline() {
    }

    public Discipline(String name, Integer idDepartment) {
        this.name = name;
        this.idDepartment = idDepartment;
    }

    public Discipline(Integer id, String name, Integer idDepartment) {
        this.id = id;
        this.name = name;
        this.idDepartment = idDepartment;
    }

    public Discipline(Integer id, String name, Integer idDepartment, String name_department) {
        this.id = id;
        this.name = name;
        this.idDepartment = idDepartment;
        this.name_department = name_department;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIdDepartment() {
        return idDepartment;
    }

    public void setIdDepartment(Integer idDepartment) {
        this.idDepartment = idDepartment;
    }

    public String getName_department() {
        return name_department;
    }

    public void setName_department(String name_department) {
        this.name_department = name_department;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Discipline that = (Discipline) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (idDepartment != null ? !idDepartment.equals(that.idDepartment) : that.idDepartment != null) return false;
        return name_department != null ? name_department.equals(that.name_department) : that.name_department == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (idDepartment != null ? idDepartment.hashCode() : 0);
        result = 31 * result + (name_department != null ? name_department.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Discipline{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", idDepartment=" + idDepartment +
                ", name_department='" + name_department + '\'' +
                '}';
    }
}
