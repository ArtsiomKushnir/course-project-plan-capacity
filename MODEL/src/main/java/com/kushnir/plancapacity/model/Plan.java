package com.kushnir.plancapacity.model;


public class Plan {
    private String name_teacher;
    private Semestr semestr;

    public Plan() {
    }

    public Plan(String name_teacher, Semestr semestr) {
        this.name_teacher = name_teacher;
        this.semestr = semestr;
    }

    public String getName_teacher() {
        return name_teacher;
    }

    public void setName_teacher(String name_teacher) {
        this.name_teacher = name_teacher;
    }

    public Semestr getSemestr() {
        return semestr;
    }

    public void setSemestr(Semestr semestr) {
        this.semestr = semestr;
    }
}
