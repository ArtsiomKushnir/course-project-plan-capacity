package com.kushnir.plancapacity.model;

/**
 * Right class
 */
public class Right {

    private Integer id;
    private String name;

    public Right() {
    }

    public Right(String name) {
        this.name = name;
    }

    public Right(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Right right = (Right) o;

        if (!id.equals(right.id)) return false;
        return name.equals(right.name);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }


}
