package com.kushnir.plancapacity.service.test;

import com.kushnir.plancapacity.model.User;
import com.kushnir.plancapacity.service.UserService;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User Service Impl Test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-service-config-test.xml"})
@Transactional
public class UserServiceImplTest {

    @Autowired
    UserService userService;

    @Test
    public void getAllUsers() throws Exception {
        List<User> users = userService.getAllUsers();
        Assert.assertEquals("", 4, users.size());
    }

}
