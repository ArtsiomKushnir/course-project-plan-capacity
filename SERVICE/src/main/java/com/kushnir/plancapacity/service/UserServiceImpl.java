package com.kushnir.plancapacity.service;

import com.kushnir.plancapacity.dao.UserDAO;
import com.kushnir.plancapacity.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * User Service implementation
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserDAO userDAO;

    @Override
    public List<User> getAllUsers() throws DataAccessException {
        return userDAO.getAllUsers();
    }
}
