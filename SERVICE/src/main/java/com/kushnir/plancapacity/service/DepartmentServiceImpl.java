package com.kushnir.plancapacity.service;

import com.kushnir.plancapacity.dao.DepartmentDAO;
import com.kushnir.plancapacity.model.Department;

import com.kushnir.plancapacity.model.Faculty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Department Service Implementation
 */
@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    private final Logger LOGGER = LogManager.getLogger();

    @Autowired
    DepartmentDAO departmentDao;

    @Autowired
    FacultyService facultyService;

    @Override
    public List<Department> getAllDepartments() throws DataAccessException {
        List<Department> departments = departmentDao.getAllDepartments();
        LOGGER.debug("getAllDepartments(){}", departments);
        return departments;
    }

    @Override
    public List<Department> getAllDepartmentsByFaculty(int facultyId) throws DataAccessException {
        Assert.notNull(facultyId, "ID Факультета не должно быть NULL");
        Assert.isTrue(facultyId > 0, "ID Факультета не должно быть меньше или равно нулю");
        List<Department> departments = departmentDao.getAllDepartmentsByFaculty(facultyId);
        LOGGER.debug("getAllDepartmentsByFaculty("+facultyId+"){}", departments);
        return departments;
    }

    @Override
    public List<Faculty> getAllFaculties() throws DataAccessException {
        List<Faculty> faculties = facultyService.getAllFaculties();
        LOGGER.debug("getAllFaculties(){}", faculties);
        return faculties;
    }

    @Override
    public Department getDepartmentById(int id) throws DataAccessException {
        Assert.notNull(id, "ID не должно быть NULL");
        Assert.isTrue(id > 0, "ID не должно быть меньше или равно нулю");
        try {
            Department department = departmentDao.getDepartmentById(id);
            LOGGER.debug("getDepartmentById("+id+"){}", department);
            return department;
        } catch (Exception e) {
            LOGGER.error("getDepartmentById("+id+") ERROR >>", e);
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public Integer addDepartment(Department department) throws DataAccessException {
        LOGGER.debug("addDepartment(department: {})", department);
        Assert.notNull(department, "Объект Department не должен быть NULL");
        Assert.hasText(department.getName(), "Название новой кафедры не должно быть пустым");
        Assert.notNull(department.getId_faculty(), "ID Факультета не должен быть NULL");
        Assert.isTrue(department.getId_faculty() > 0,
                "ID Факультета не должен быть меньше или равен нулю");
        try {
            return departmentDao.addDepartment(department);
        } catch (DuplicateKeyException e) {
            LOGGER.error("addDepartment() ERROR >>", e);
            throw new DuplicateKeyException("Кафедра: "+department.getName()+ " - уже присутствует в справочнике!");
        }
    }

    @Override
    public void updateDepartment(Department department) throws DataAccessException {
        LOGGER.debug("updateDepartment(department: {})", department);
        Assert.notNull(department, "Объект Department не должен быть NULL");
        Assert.notNull(department.getId(), "ID Кафедры не должен быть NULL");
        Assert.isTrue(department.getId() > 0, "ID Кафедры не должен быть меньше или равен нулю");
        Assert.notNull(department.getId_faculty(), "ID Факультета не должен быть NULL");
        Assert.isTrue(department.getId_faculty() > 0,
                "ID Факультета не должен быть меньше или равен нулю");
        Assert.hasText(department.getName(), "Название нового Кафедры не должно быть пустым");
        try {
            departmentDao.updateDepartment(department);
        } catch (Exception e) {
            LOGGER.error("updateDepartment() ERROR >>", e);
            throw new DataIntegrityViolationException("Невозможно обновить!, Кафедра не найдена!");
        }
    }

    @Override
    public void deleteDepartment(int id) throws DataAccessException {
        LOGGER.debug("deleteDepartment({})", id);
        Assert.notNull(id, "ID Кафедры не должен быть NULL");
        Assert.isTrue(id > 0, "ID Кафедры не должен быть меньше или равен нулю");
        try {
            departmentDao.deleteDepartment(id);
        } catch (DataIntegrityViolationException e) {
            LOGGER.error("deleteDepartment() ERROR >>", e);
            throw new DataIntegrityViolationException("Невозможно удалить кафедру, так как у него есть подчиненные завичимости! (преподаватели и/или предметы)");
        }
    }

    @Override
    public void deleteDepartmentByFaculty(int facultyId) throws DataAccessException {
        LOGGER.debug("deleteDepartmentByFaculty({})", facultyId);
        Assert.notNull(facultyId, "ID Факультета не должно быть NULL");
        Assert.isTrue(facultyId > 0, "ID Факультета не должно быть меньше или равно нулю");
        try {
            departmentDao.deleteDepartmentByFaculty(facultyId);
        } catch (DataIntegrityViolationException e) {
            LOGGER.error("deleteDepartmentByFaculty() ERROR >>", e);
            throw new DataIntegrityViolationException("Невозможно удалить кафедры, так как есть подчиненные завичимости! (преподаватели и/или предметы)");
        }
    }
}
