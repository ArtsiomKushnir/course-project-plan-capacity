package com.kushnir.plancapacity.service;

import com.kushnir.plancapacity.dao.SpecialtyDAO;
import com.kushnir.plancapacity.model.Faculty;
import com.kushnir.plancapacity.model.Specialty;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.util.Assert;

import java.util.List;

/**
 * SpecialtyService Implementation
 */
public class SpecialtyServiceImpl implements SpecialtyService {

    private final Logger LOGGER = LogManager.getLogger();

    @Autowired
    private SpecialtyDAO specialtyDao;

    @Autowired
    FacultyService facultyService;

    @Override
    public List<Specialty> getAllSpecialty() throws DataAccessException {
        List<Specialty> specialties = specialtyDao.getAllSpecialty();
        LOGGER.debug("getAllSpecialty(){}", specialties);
        return specialties;
    }

    @Override
    public List<Specialty> getAllSpecialtyByFaculty(int facultyId) throws DataAccessException {
        Assert.notNull(facultyId, "ID Факультета не должно быть NULL");
        Assert.isTrue(facultyId > 0, "ID Факультета не должно быть меньше или равно нулю");
        List<Specialty> specialties = specialtyDao.getSpecialtyByFaculty(facultyId);
        LOGGER.debug("getAllSpecialtyByFaculty("+facultyId+"){}", specialties);
        return specialties;
    }

    @Override
    public List<Faculty> getAllFaculties() throws DataAccessException {
        List<Faculty> faculties = facultyService.getAllFaculties();
        LOGGER.debug("getAllFaculties(){}", faculties);
        return faculties;
    }

    @Override
    public Specialty getSpecialtyById(int id) throws DataAccessException {
        Assert.notNull(id, "ID не должно быть NULL");
        Assert.isTrue(id > 0, "ID не должно быть меньше или равно нулю");
        try {
            Specialty specialty = specialtyDao.getSpecialtyById(id);
            LOGGER.debug("getSpecialtyById("+id+"){}", specialty);
            return specialty;
        } catch (Exception e) {
            LOGGER.error("getSpecialtyById("+id+") ERROR >>", e);
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public Integer addSpecialty(Specialty specialty) throws DataAccessException {
        LOGGER.debug("addSpecialty() {}", specialty);
        Assert.notNull(specialty, "Объект Specialty не должен быть NULL");
        Assert.hasText(specialty.getName(), "Название новой специальности не должно быть пустым");
        Assert.hasText(specialty.getCode(), "Код новой специальности не должен быть пустым");
        Assert.notNull(specialty.getId_faculty(), "ID Факультета не должен быть NULL");
        Assert.isTrue(specialty.getId_faculty() > 0,
                "ID Факультета не должен быть меньше или равен нулю");
        try {
            return specialtyDao.addSpecialty(specialty);
        } catch (DuplicateKeyException e) {
            LOGGER.error("addSpecialty() ERROR >>", e);
            throw new DuplicateKeyException("Специальность: "+specialty.getName()+ " - уже присутствует в справочнике!");
        }
    }

    @Override
    public void updateSpecialty(Specialty specialty) throws DataAccessException {
        LOGGER.debug("updateSpecialty() {}", specialty);
        Assert.notNull(specialty, "Объект Specialty не должен быть NULL");
        Assert.notNull(specialty.getId(), "ID Специальности не должен быть NULL");
        Assert.isTrue(specialty.getId() > 0, "ID Специальности не должен быть меньше или равен нулю");
        Assert.hasText(specialty.getName(), "Название новой специальности не должно быть пустым");
        Assert.hasText(specialty.getCode(), "Код новой специальности не должен быть пустым");
        Assert.notNull(specialty.getId_faculty(), "ID Факультета не должен быть NULL");
        Assert.isTrue(specialty.getId_faculty() > 0,
                "ID Факультета не должен быть меньше или равен нулю");
        try {
            specialtyDao.updateSpecialty(specialty);
        } catch (DuplicateKeyException e) {
            LOGGER.error("updateSpecialty() ERROR >>", e);
            throw new DuplicateKeyException("Невозможно обновить!, Специальность не найдена!");
        }
    }

    @Override
    public void deleteSpecialty(int id) throws DataAccessException {
        LOGGER.debug("deleteSpecialty({})", id);
        Assert.notNull(id, "Объект Specialty не должен быть NULL");
        Assert.notNull(id > 0, "ID Специальности не должен быть NULL");
        try {
            specialtyDao.deleteSpecialty(id);
        } catch (Exception e) {
            LOGGER.error("deleteSpecialty() ERROR >>", e);
            throw new DataIntegrityViolationException("Ошибка при удалении специальности!");
        }
    }

    @Override
    public void deleteSpecialtyByFaculty(int facultyId) throws DataAccessException {
        Assert.notNull(facultyId, "ID Факультета не должно быть NULL");
        Assert.isTrue(facultyId > 0, "ID Факультета не должно быть меньше или равно нулю");
        try {
            specialtyDao.deleteSpecialtyByFaculty(facultyId);
        } catch (Exception e) {
            LOGGER.error("deleteSpecialtyByFaculty() ERROR >>", e);
            throw new DataIntegrityViolationException("Ошибка при удалении специальности!");
        }
    }
}
