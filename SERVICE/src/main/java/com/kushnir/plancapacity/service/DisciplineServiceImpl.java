package com.kushnir.plancapacity.service;

import com.kushnir.plancapacity.dao.DisciplineDAO;
import com.kushnir.plancapacity.model.Department;
import com.kushnir.plancapacity.model.Discipline;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Discipline Service Implementation
 */
public class DisciplineServiceImpl implements DisciplineService {

    private final Logger LOGGER = LogManager.getLogger();

    @Autowired
    DisciplineDAO disciplineDao;

    @Autowired
    DepartmentService departmentService;

    @Override
    public List<Discipline> getAllDisciplines() throws DataAccessException {
        List<Discipline> disciplines = disciplineDao.getAllDisciplines();
        LOGGER.debug("getAllDisciplines(){}", disciplines);
        return disciplines;
    }

    @Override
    public List<Discipline> getAllDisciplinesByDepartment(int idDepartment) throws DataAccessException {
        Assert.notNull(idDepartment, "ID Кафедры не должно быть NULL");
        Assert.isTrue(idDepartment > 0, "ID Кафедры не должно быть меньше или равно нулю");
        List<Discipline> disciplines = disciplineDao.getAllDisciplinesByDepartment(idDepartment);
        LOGGER.debug("getAllDisciplinesByDepartment("+idDepartment+"){}", disciplines);
        return disciplines;
    }

    @Override
    public List<Department> getAllDepartments() throws DataAccessException {
        return departmentService.getAllDepartments();
    }

    @Override
    public Discipline getDisciplineById(int idDiscipline) throws DataAccessException {
        Assert.notNull(idDiscipline, "ID Предмета не должно быть NULL");
        Assert.isTrue(idDiscipline > 0, "ID Предмета не должно быть меньше или равно нулю");
        try {
            Discipline discipline = disciplineDao.getDisciplineById(idDiscipline);
            LOGGER.debug("getDisciplineById("+idDiscipline+"){}", discipline);
            return discipline;
        } catch (Exception e) {
            LOGGER.error("getDisciplineById("+idDiscipline+") ERROR >> ", e);
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public Integer addDiscipline(Discipline discipline) throws DataAccessException {
        LOGGER.debug("addDiscipline() {}", discipline);
        Assert.notNull(discipline, "Объект Discipline не должен быть NULL");
        Assert.hasText(discipline.getName(), "Назвавние нового предмета не должно быть пустым");
        Assert.isTrue(discipline.getIdDepartment() > 0,
                "ID Кафедры не должен быть меньше или равен нулю");
        try {
            return disciplineDao.addDiscipline(discipline);
        } catch (DuplicateKeyException e) {
            LOGGER.error("addDiscipline() ERROR >>", e);
            throw new DuplicateKeyException("Предмет: "+discipline.getName()+ " - уже присутствует в справочнике!");
        }
    }

    @Override
    public void updateDiscipline(Discipline discipline) throws DataAccessException {
        LOGGER.debug("updateDiscipline() {}", discipline);
        Assert.notNull(discipline, "Объект Discipline не должен быть NULL");
        Assert.isTrue(discipline.getId() > 0, "ID предмета не должен быть меньше или равен нулю");
        Assert.hasText(discipline.getName(), "Назвавние ноаого предмета не должно быть пустым");
        Assert.isTrue(discipline.getIdDepartment() > 0,
                "ID Кафедры не должен быть меньше или равен нулю");
        try {
            disciplineDao.updateDiscipline(discipline);
        } catch (DuplicateKeyException e) {
            LOGGER.error("updateDiscipline() ERROR >>", e);
            throw new DuplicateKeyException("Предмет: "+discipline.getName()+ " - уже присутствует в справочнике!");
        }
    }

    @Override
    public void deleteDiscipline(int idDiscipline) throws DataAccessException {
        Assert.isTrue(idDiscipline > 0, "ID предмета не должно быть меньше или равно нулю");
        try {
            disciplineDao.deleteDiscipline(idDiscipline);
        } catch (Exception e) {
            LOGGER.error("deleteDiscipline() ERROR >>", e);
            throw new DataIntegrityViolationException("Ошибка при удалении Предмета!");
        }
    }

    @Override
    public void deleteDisciplineByDepartment(int idDepartment) throws DataAccessException {
        Assert.isTrue(idDepartment > 0, "ID Кафедры не должно быть меньше или равно нулю");
        try {
            disciplineDao.deleteDisciplineByDepartment(idDepartment);
        } catch (Exception e) {
            LOGGER.error("deleteDisciplineByDepartment() ERROR >>", e);
            throw new DataIntegrityViolationException("Ошибка при удалении Предмета!");
        }
    }

}
