package com.kushnir.plancapacity.service;

import com.kushnir.plancapacity.dao.TeacherDAO;
import com.kushnir.plancapacity.model.Department;
import com.kushnir.plancapacity.model.Teacher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.util.Assert;

import java.util.List;

/**
 * TeacherService implementation
 */
public class TeacherServiceImpl implements TeacherService {

    private static final Logger LOGGER = LogManager.getLogger(TeacherServiceImpl.class);

    @Autowired
    TeacherDAO teacherDao;

    @Autowired
    DepartmentService departmentService;

    @Override
    public List<Teacher> getAllTeachers() throws DataAccessException {
        List<Teacher> teachers = teacherDao.getAllTeachers();
        LOGGER.debug("getAllTeachers(){}", teachers);
        return teachers;
    }

    @Override
    public List<Teacher> getAllTeachersByDepartment(int idDepartment) throws DataAccessException {
        Assert.notNull(idDepartment, "ID Кафедры не должно быть NULL");
        Assert.isTrue(idDepartment > 0, "ID Кафедры не должно быть меньше или равно нулю");
        List<Teacher> teachers = teacherDao.getAllTeachersByDepartment(idDepartment);
        LOGGER.debug("getAllTeachersByDepartment("+idDepartment+"){}", teachers);
        return teachers;
    }

    @Override
    public List<Department> getAllDepartments() throws DataAccessException {
        return departmentService.getAllDepartments();
    }

    @Override
    public Teacher getTeacherById(int idTeacher) throws DataAccessException {
        Assert.notNull(idTeacher, "ID Преподавателя не должно быть NULL");
        Assert.isTrue(idTeacher > 0, "ID Преподавателя не должно быть меньше или равно нулю");
        try {
            Teacher teacher = teacherDao.getTeacherById(idTeacher);
            LOGGER.debug("getTeacherById("+idTeacher+"){}", teacher);
            return teacher;
        } catch (Exception e) {
            LOGGER.error("getTeacherById("+idTeacher+") ERROR >>", e);
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public Integer addTeacher(Teacher teacher) throws DataAccessException {
        LOGGER.debug("addTeacher() {}", teacher);
        Assert.notNull(teacher, "Объект Teacher не должен быть NULL");
        Assert.hasText(teacher.getName(), "ФИО нового преподавателя не должно быть пустым");
        Assert.isTrue(teacher.getDepartment() > 0,
                "ID Кафедры не должен быть меньше или равен нулю");
        try {
            return teacherDao.addTeacher(teacher);
        } catch (DuplicateKeyException e) {
            LOGGER.error("addTeacher() ERROR >>", e);
            throw new DuplicateKeyException("Преподаватель: "+teacher.getName()+ " - уже присутствует в справочнике!");
        }
    }

    @Override
    public void updateTeacher(Teacher teacher) throws DataAccessException {
        LOGGER.debug("updateTeacher() {}", teacher);
        Assert.notNull(teacher, "Объект Teacher не должен быть NULL");
        Assert.isTrue(teacher.getId() > 0, "ID преподавателя не должен быть меньше или равен нулю");
        Assert.hasText(teacher.getName(), "ФИО нового преподавателя не должно быть пустым");
        Assert.isTrue(teacher.getDepartment() > 0,
                "ID Кафедры не должен быть меньше или равен нулю");
        try {
            teacherDao.updateTeacher(teacher);
        } catch (DuplicateKeyException e) {
            LOGGER.error("updateTeacher() ERROR >>", e);
            throw new DuplicateKeyException("Преподаватель: "+teacher.getName()+ " - уже присутствует в справочнике!");
        }
    }

    @Override
    public void deleteTeacher(int idTeacher) throws DataAccessException {
        Assert.isTrue(idTeacher > 0, "ID Преподавателя не должно быть меньше или равно нулю");
        try {
            teacherDao.deleteTeacher(idTeacher);
        } catch (Exception e) {
            LOGGER.error("deleteTeacher() ERROR >>", e);
            throw new DataIntegrityViolationException("Ошибка при удалении Преподавателя!");
        }
    }

    @Override
    public void deleteTeachersByDepartment(int idDepartment) throws DataAccessException {
        Assert.isTrue(idDepartment > 0, "ID Кафедры не должно быть меньше или равно нулю");
        try {
            teacherDao.deleteTeachersByDepartment(idDepartment);
        } catch (Exception e) {
            LOGGER.error("deleteTeachersByDepartment() ERROR >>", e);
            throw new DataIntegrityViolationException("Ошибка при удалении Преподавателя!");
        }
    }
}
