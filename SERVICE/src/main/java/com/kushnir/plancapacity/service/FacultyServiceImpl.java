package com.kushnir.plancapacity.service;

import com.kushnir.plancapacity.dao.FacultyDAO;
import com.kushnir.plancapacity.model.Faculty;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * FacultyService implementation
 */
@Service
@Transactional
public class FacultyServiceImpl implements FacultyService {

    private final Logger LOGGER = LogManager.getLogger();

    @Autowired
    FacultyDAO facultyDao;

    @Override
    public List<Faculty> getAllFaculties() throws DataAccessException {
        LOGGER.debug("getAllFaculties()");
        return facultyDao.getAllFaculties();
    }

    @Override
    public Faculty getFaculty(int id) throws DataAccessException {
        LOGGER.debug("getFaculty(name: {})", id);
        Assert.notNull(id, "ID не должно быть NULL");
        Assert.isTrue(id > 0, "ID не должно быть меньше или равно нулю");
        return facultyDao.getFaculty(id);
    }

    @Override
    public Faculty getFacultyByName(String name) throws DataAccessException {
        LOGGER.debug("getFacultyByName(name: {})", name);
        Assert.notNull(name, "Имя не должно быть NULL");
        Assert.hasText(name, "Имя должно содержать текст");
        try {
            return facultyDao.getFacultyByName(name);
        } catch (Exception e) {
            LOGGER.error("getFacultyByName("+name+") ERROR >>", e);
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    @Override
    public Integer addFaculty(Faculty faculty) throws DataAccessException {
        LOGGER.debug("addFaculty(faculty: {})", faculty);
        Assert.notNull(faculty, "Объект Faculty не должен быть NULL");
        Assert.hasText(faculty.getName(), "Название нового факультета не должно быть пустым");
        try {
            return facultyDao.addFaculty(faculty);
        } catch (DuplicateKeyException e) {
            throw new DuplicateKeyException("Факультет: "+faculty.getName()+ " - уже присутствует в справочнике!");
        }
    }

    @Override
    public void updateFaculty(Faculty faculty) throws DataAccessException {
        LOGGER.debug("updateFaculty(faculty: {})", faculty);
        Assert.notNull(faculty, "Объект Faculty не должен быть NULL");
        Assert.notNull(faculty.getId(), "ID Факультета не должен быть NULL");
        Assert.isTrue(faculty.getId() > 0, "ID Факультета не должно быть меньше или равно нулю");
        Assert.hasText(faculty.getName(), "Название нового факультета не должно быть пустым");
        try {
            facultyDao.updateFaculty(faculty);
        } catch (DuplicateKeyException e) {
            throw new DuplicateKeyException("Факультет: "+faculty.getName()+ " - уже присутствует в справочнике!");
        }
    }

    @Override
    public void deleteFaculty(int id) throws DataAccessException {
        Assert.notNull(id, "ID Факультета не должен быть NULL");
        Assert.isTrue(id > 0, "ID Факультета не должно быть меньше или равно нулю");
        try {
            facultyDao.deleteFaculty(id);
        } catch (DataIntegrityViolationException e) {
            throw new DataIntegrityViolationException("Невозможно удалить факультет, так как у него есть подчиненные кафедры и/или специальности!");
        }
    }
}
