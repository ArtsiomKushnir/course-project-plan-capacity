package com.kushnir.plancapacity.fxapp;

import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;

/**
 * Popup Dialog
 */
public class PopupDialog extends Dialog {

    PopupDialog(Object o, String msg) {
        setTitle(msg);
        setHeaderText(null);
        getDialogPane().getButtonTypes().addAll(new ButtonType("СОХРАНИТЬ", ButtonBar.ButtonData.OK_DONE));
    }
}
