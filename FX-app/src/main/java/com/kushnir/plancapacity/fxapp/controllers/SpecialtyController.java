package com.kushnir.plancapacity.fxapp.controllers;

import com.kushnir.plancapacity.fxapp.MainClass;
import com.kushnir.plancapacity.fxapp.PopupAlert;
import com.kushnir.plancapacity.model.Faculty;
import com.kushnir.plancapacity.model.Specialty;
import com.kushnir.plancapacity.service.SpecialtyService;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.Assert;

import java.io.IOException;

/**
 * Specialty View and Control Class
 */
public class SpecialtyController {

    private final Logger LOGGER = LogManager.getLogger();
    private final PopupAlert alertInfo = new PopupAlert(Alert.AlertType.INFORMATION);
    private final PopupAlert alertError = new PopupAlert(Alert.AlertType.ERROR);
    private SpecialtyService specialtyService =
            (SpecialtyService) MainClass.applicationContext.getBean("specialtyService");

    @FXML private TextField newSpecialtyName;
    @FXML private TextField newSpecialtyCode;
    @FXML private ComboBox<Faculty> combo;
    @FXML private TableView<Specialty> table;
    @FXML private TableColumn editName;
    @FXML private TableColumn editCode;
    @FXML private TableColumn action;

    @FXML
    public void initialize() throws Exception {
        LOGGER.debug("initialize()");
        this.fill();
    }

    public void fill() {
        this.initComboBoxFaculties();
        this.fillTable();
    }

    public void fillTable() {
        try{
            this.addButtonsToTable();
            this.setEditColumn();
            table.setItems(FXCollections.observableArrayList(this.specialtyService.getAllSpecialty()));
        } catch (Exception e) {
            LOGGER.error("fillTable() ERROR! >>", e);
        }
    }

    public void initComboBoxFaculties() {
        combo.setConverter(new StringConverter<Faculty>() {
            @Override
            public String toString(Faculty faculty) {
                return faculty.getName();
            }
            @Override
            public Faculty fromString(String string) {
                return null;
            }
        });
        combo.getItems().removeAll();
        combo.getItems().addAll(FXCollections.observableArrayList(this.specialtyService.getAllFaculties()));
    }

    @FXML
    public void handleBtnAddSpecialty(ActionEvent event) throws IOException {
        LOGGER.debug("handleBtnAddSpecialty(ActionEvent: {})", event);
        try {
            Assert.hasText(newSpecialtyName.getText(),"Введите название Специальности");
            Assert.hasText(newSpecialtyCode.getText(),"Введите код Специальности");
            Assert.notNull(combo.getSelectionModel().getSelectedItem(),"Не выбран факультет!");
            Integer id = this.specialtyService.addSpecialty(new Specialty(newSpecialtyName.getText(),
                    newSpecialtyCode.getText(),
                    combo.getSelectionModel().getSelectedItem().getId()));
            alertInfo.showAlert("ДОБАВЛЕНА НОВАЯ СПЕЦИАЛЬНОСТЬ", null, "ДОБАВЛЕНА НОВАЯ СПЕЦИАЛЬНОСТЬ: "
                    + "ID(" + id +") \""+ newSpecialtyName.getText() + "\"");
            this.fillTable();
        } catch (Exception e) {
            alertError.showAlert("ОШИБКА ДОБАВЛЕНИЯ СПЕЦИАЛЬНОСТИ!", null, e.getMessage());
            LOGGER.error("handleBtnAddSpecialty() ERROR! >>", e);
        }
    }


    private void addButtonsToTable() {
        action.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Specialty, Boolean>,
                ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Specialty, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        action.setCellFactory(new Callback<TableColumn<Specialty, Boolean>, TableCell<Specialty, Boolean>>() {
            @Override
            public TableCell<Specialty, Boolean> call(TableColumn<Specialty, Boolean> p) {
                return new ButtonCell();
            }
        });
    }

    //Define the button cell
    private class ButtonCell extends TableCell<Specialty, Boolean> {

        final Button cellButton = new Button("УДАЛИТЬ");

        ButtonCell(){
            //Action when the button is pressed
            cellButton.setOnAction(new EventHandler<ActionEvent>(){
                @Override
                public void handle(ActionEvent t) {
                    LOGGER.debug("handleBtnDeleteSpecialty(ActionEvent: {})", t);
                    // get Selected Item
                    Specialty currentSpecialty =
                            (Specialty) ButtonCell.this.getTableView().getItems().get(ButtonCell.this.getIndex());
                    //remove selected item from the table list
                    try {
                        specialtyService.deleteSpecialty(currentSpecialty.getId());
                        alertInfo.showAlert("СПЕЦИАЛЬНОСТЬ УДАЛЕНА", null, "СПЕЦИАЛЬНОСТЬ ("
                                +"\""+ currentSpecialty.getName() + "\") - удалена!");
                        fillTable();
                    } catch (Exception e) {
                        alertError.showAlert("ОШИБКА УДАЛЕНИЯ СПЕЦИАЛЬНОСТИ!", null, e.getMessage());
                        LOGGER.error("handleBtnDeleteSpecialty() ERROR! >>", e);
                    }
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(cellButton);
            }
        }
    }

    public void setEditColumn() {
        Callback<TableColumn, TableCell> cellFactory = new Callback<TableColumn, TableCell>() {
            public TableCell call(TableColumn p) {
                return new EditingCell();
            }
        };

        editName.setCellFactory(cellFactory);
        editName.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Specialty, String>>() {
            @Override public void handle(TableColumn.CellEditEvent<Specialty, String> t) {

                LOGGER.debug("setEditColumn({})", t);
                try {
                    Specialty editSpecialty = t.getRowValue();
                    editSpecialty.setName(t.getNewValue());
                    specialtyService.updateSpecialty(editSpecialty);
                    alertInfo.showAlert(
                            "СПЕЦИАЛЬНОСТ ОБНОВЛЕНА!",
                            null,
                            "НАЗВАНИЕ СПЕЦИАЛЬНОСТИ ИЗМЕНЕНО НА: \""+ editSpecialty.getName() + "\"");
                    /*((Faculty)t.getTableView().getItems().get(
                            t.getTablePosition().getRow())).setName(t.getNewValue());*/
                } catch (Exception e) {
                    alertError.showAlert("ОШИБКА ОБНОВЛЕНИЯ СПЕЦИАЛЬНОСТИ!", null, e.getMessage());
                    LOGGER.error("setEditColumn() ERROR! >>", e);
                }
            }
        });

        editCode.setCellFactory(cellFactory);
        editCode.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Specialty, String>>() {
            @Override public void handle(TableColumn.CellEditEvent<Specialty, String> t) {

                LOGGER.debug("setEditColumn({})", t);
                try {
                    Specialty editSpecialty = t.getRowValue();
                    editSpecialty.setCode(t.getNewValue());
                    specialtyService.updateSpecialty(editSpecialty);
                    alertInfo.showAlert(
                            "СПЕЦИАЛЬНОСТ ОБНОВЛЕНА!",
                            null,
                            "КОД СПЕЦИАЛЬНОСТИ ИЗМЕНЕНО НА: \""+ editSpecialty.getCode() + "\"");
                    /*((Faculty)t.getTableView().getItems().get(
                            t.getTablePosition().getRow())).setName(t.getNewValue());*/
                } catch (Exception e) {
                    alertError.showAlert("ОШИБКА ОБНОВЛЕНИЯ СПЕЦИАЛЬНОСТИ!", null, e.getMessage());
                    LOGGER.error("setEditColumn() ERROR! >>", e);
                }
            }
        });
    }

    class EditingCell extends TableCell<Specialty, String> {

        private TextField textField;
        public EditingCell() {}

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()*2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }

}
