package com.kushnir.plancapacity.fxapp;

import com.kushnir.plancapacity.fxapp.controllers.MainWindowController;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Main class of Java FX application
 */
public class MainClass extends Application {

    public static ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/spring-fxapp-config.xml");

    public static void main(String[] args) throws Exception {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        primaryStage.setTitle("АИС Учебный план");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/mainWindow.fxml"));
        primaryStage.setScene(new Scene(loader.load()));
        MainWindowController mainController = loader.getController();

        mainController.initialize();

        primaryStage.show();
    }
}
