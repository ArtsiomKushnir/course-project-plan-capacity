package com.kushnir.plancapacity.fxapp;

import javafx.beans.NamedArg;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 * Custom popup Alert Class
 */
public class PopupAlert extends Alert {
    public PopupAlert(@NamedArg("alertType") AlertType alertType) {
        super(alertType);
    }

    public PopupAlert(@NamedArg("alertType") AlertType alertType, @NamedArg("contentText") String contentText, ButtonType... buttons) {
        super(alertType, contentText, buttons);
    }

    public void showAlert(String title , String header, String content) {
        super.setTitle(title);
        super.setHeaderText(header);
        super.setContentText(content);
        super.showAndWait();
    }
}
