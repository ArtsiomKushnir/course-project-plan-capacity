package com.kushnir.plancapacity.fxapp.controllers;

import com.kushnir.plancapacity.fxapp.MainClass;
import com.kushnir.plancapacity.fxapp.PopupAlert;
import com.kushnir.plancapacity.model.Department;
import com.kushnir.plancapacity.model.Faculty;
import com.kushnir.plancapacity.service.DepartmentService;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.util.Assert;

import java.io.IOException;

/**
 * Departments View and Control Class
 */
public class DepartmentsController {

    private final Logger LOGGER = LogManager.getLogger();
    private final PopupAlert alertInfo = new PopupAlert(Alert.AlertType.INFORMATION);
    private final PopupAlert alertError = new PopupAlert(Alert.AlertType.ERROR);

    private DepartmentService departmentService = (DepartmentService) MainClass.applicationContext.getBean("departmentService");

    @FXML private TextField newDepartmentName;
    @FXML private ComboBox<Faculty> combo;
    @FXML private TableView<Department> table;
    @FXML private TableColumn editColumn;
    @FXML private TableColumn action;

    @FXML
    public void initialize() throws Exception {
        LOGGER.debug("initialize()");
        this.fill();
    }

    public void fill() {
        this.initComboBoxFaculties();
        this.fillTable();
    }

    public void fillTable() {
        try{
            this.addButtonsToTable();
            this.setEditColumn();
            table.setItems(FXCollections.observableArrayList(this.departmentService.getAllDepartments()));
        } catch (Exception e) {
            LOGGER.error("fillTable() ERROR! >>", e);
        }
    }

    public void initComboBoxFaculties() {
        combo.setConverter(new StringConverter<Faculty>() {
            @Override
            public String toString(Faculty object) {
                return object.getName();
            }
            @Override
            public Faculty fromString(String string) {
                return null;
            }
        });
        combo.getItems().removeAll();
        combo.getItems().addAll(FXCollections.observableArrayList(this.departmentService.getAllFaculties()));
    }

    @FXML
    public void handleBtnAddDepaetment(ActionEvent event) throws IOException {
        LOGGER.debug("handleBtnAddDepaetment(ActionEvent: {})", event);
        try {
            Assert.hasText(newDepartmentName.getText(),"Введите название кафедры");
            Assert.notNull(combo.getSelectionModel().getSelectedItem(),"Не выбран факультет!");
            Integer id = departmentService.addDepartment(new Department(newDepartmentName.getText(),
                    combo.getSelectionModel().getSelectedItem().getId()));
            alertInfo.showAlert("ДОБАВЛЕНА НОВАЯ КАФЕДРА", null, "ДОБАВЛЕНА НОВАЯ КАФЕДРА: "
                    + "ID(" + id +") \""+ newDepartmentName.getText() + "\"");
            this.fillTable();
        } catch (Exception e) {
            alertError.showAlert("ОШИБКА ДОБАВЛЕНИЯ КАФЕДРЫ!", null, e.getMessage());
            LOGGER.error("handleBtnAddDepaetment() ERROR! >>", e);
        }
    }

    private void addButtonsToTable() {
        action.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Department, Boolean>,
                        ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Department, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        action.setCellFactory(new Callback<TableColumn<Department, Boolean>, TableCell<Department, Boolean>>() {
            @Override
            public TableCell<Department, Boolean> call(TableColumn<Department, Boolean> p) {
                return new ButtonCell();
            }
        });
    }

    //Define the button cell
    private class ButtonCell extends TableCell<Department, Boolean> {

        final Button cellButton = new Button("УДАЛИТЬ");

        ButtonCell(){
            //Action when the button is pressed
            cellButton.setOnAction(new EventHandler<ActionEvent>(){
                @Override
                public void handle(ActionEvent t) {
                    LOGGER.debug("handleBtnDeleteDepartment(ActionEvent: {})", t);
                    // get Selected Item
                    Department currentDepartment =
                            (Department) ButtonCell.this.getTableView().getItems().get(ButtonCell.this.getIndex());
                    //remove selected item from the table list
                    try {
                        departmentService.deleteDepartment(currentDepartment.getId());
                        alertInfo.showAlert("КАФЕДРА УДАЛЕНА", null, "КАФЕДРА ("
                                +"\""+ currentDepartment.getName() + "\") - удалена!");
                        fillTable();
                    } catch (Exception e) {
                        alertError.showAlert("ОШИБКА УДАЛЕНИЯ КАФЕДРЫ!", null, e.getMessage());
                        LOGGER.error("handleBtnDeleteDepartment() ERROR! >>", e);
                    }
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(cellButton);
            }
        }
    }

    public void setEditColumn() {

        Callback<TableColumn, TableCell> cellFactory = new Callback<TableColumn, TableCell>() {
            public TableCell call(TableColumn p) {
                return new EditingCell();
            }
        };

        editColumn.setCellFactory(cellFactory);
        editColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Department, String>>() {
            @Override public void handle(TableColumn.CellEditEvent<Department, String> t) {

                LOGGER.debug("setEditColumn({})", t);
                try {
                    Department editDepartment = t.getRowValue();
                    editDepartment.setName(t.getNewValue());
                    departmentService.updateDepartment(editDepartment);
                    alertInfo.showAlert(
                            "КАФЕДРА ОБНОВЛЕН!",
                            null,
                            "НАЗВАНИЕ КАФЕДРЫ ИЗМЕНЕНО НА: \""+ editDepartment.getName() + "\"");
                    /*((Faculty)t.getTableView().getItems().get(
                            t.getTablePosition().getRow())).setName(t.getNewValue());*/
                } catch (Exception e) {
                    alertError.showAlert("ОШИБКА ОБНОВЛЕНИЯ КАФЕДРЫ!", null, e.getMessage());
                    LOGGER.error("setEditColumn() ERROR! >>", e);
                }
            }
        });
    }

    class EditingCell extends TableCell<Department, String> {

        private TextField textField;
        public EditingCell() {}

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()*2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }


}
