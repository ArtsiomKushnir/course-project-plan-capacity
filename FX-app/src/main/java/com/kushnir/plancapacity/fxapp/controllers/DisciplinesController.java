package com.kushnir.plancapacity.fxapp.controllers;

import com.kushnir.plancapacity.fxapp.MainClass;
import com.kushnir.plancapacity.fxapp.PopupAlert;
import com.kushnir.plancapacity.model.Department;
import com.kushnir.plancapacity.model.Discipline;
import com.kushnir.plancapacity.service.DisciplineService;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.util.Assert;

import java.io.IOException;

/**
 * Disciplines View and Control Class
 */
public class DisciplinesController {

    private final Logger LOGGER = LogManager.getLogger();
    private final PopupAlert alertInfo = new PopupAlert(Alert.AlertType.INFORMATION);
    private final PopupAlert alertError = new PopupAlert(Alert.AlertType.ERROR);
    private DisciplineService disciplineService =
            (DisciplineService) MainClass.applicationContext.getBean("disciplineService");

    @FXML private TextField newDisciplineName;
    @FXML private ComboBox<Department> comboDepartments;
    @FXML private TableView<Discipline> table;
    @FXML private TableColumn editNaim;
    @FXML private TableColumn action;

    @FXML
    public void initialize() throws Exception {
        LOGGER.debug("initialize()");
        fill();
    }

    public void fill() {
        this.initComboBoxDepartments();
        fillTable();
    }

    public void fillTable() {
        try{
            this.addButtonsToTable();
            this.setEditColumn();
            table.setItems(FXCollections.observableArrayList(this.disciplineService.getAllDisciplines()));
        } catch (Exception e) {
            LOGGER.error("fillTable() ERROR! >>", e);
        }
    }

    public void initComboBoxDepartments() {
        comboDepartments.setConverter(new StringConverter<Department>() {
            @Override
            public String toString(Department department) {
                return department.getName();
            }
            @Override
            public Department fromString(String string) {
                return null;
            }
        });
        comboDepartments.getItems().removeAll();
        comboDepartments.getItems().addAll(FXCollections.observableArrayList(this.disciplineService.getAllDepartments()));
    }

    @FXML
    public void handleBtnAddDiscipline(ActionEvent event) throws IOException {
        LOGGER.debug("handleBtnAddDiscipline(ActionEvent: {})", event);
        try {
            Assert.hasText(newDisciplineName.getText(),"Введите Название предмета");
            Assert.notNull(comboDepartments.getSelectionModel().getSelectedItem(),"Не выбрана кафедра!");
            Integer id = this.disciplineService.addDiscipline(new Discipline(newDisciplineName.getText(),
                    comboDepartments.getSelectionModel().getSelectedItem().getId()));
            alertInfo.showAlert("ДОБАВЛЕН НОВЫЙ ПРЕДМЕТ", null, "ДОБАВЛЕН НОВЫЙ ПРЕДМЕТ: "
                    + "ID(" + id +") \""+ newDisciplineName.getText() + "\"");
            fillTable();
        } catch (Exception e) {
            alertError.showAlert("ОШИБКА ДОБАВЛЕНИЯ ПРЕДМЕТА!", null, e.getMessage());
            LOGGER.error("handleBtnAddDiscipline() ERROR! >>", e);
        }
    }

    private void addButtonsToTable() {
        action.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Discipline, Boolean>,
                        ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Discipline, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        action.setCellFactory(new Callback<TableColumn<Discipline, Boolean>, TableCell<Discipline, Boolean>>() {
            @Override
            public TableCell<Discipline, Boolean> call(TableColumn<Discipline, Boolean> p) {
                return new ButtonCell();
            }
        });
    }

    private class ButtonCell extends TableCell<Discipline, Boolean> {

        final Button cellButton = new Button("УДАЛИТЬ");

        ButtonCell(){
            cellButton.setOnAction(new EventHandler<ActionEvent>(){
                @Override
                public void handle(ActionEvent t) {
                    LOGGER.debug("handleBtnDeleteDiscipline(ActionEvent: {})", t);
                    Discipline currentDiscipline =
                            (Discipline) ButtonCell.this.getTableView().getItems().get(ButtonCell.this.getIndex());
                    try {
                        disciplineService.deleteDiscipline(currentDiscipline.getId());
                        alertInfo.showAlert("ПРЕДМЕТ УДАЛЕН", null, "ПРЕДМЕТ ("
                                +"\""+ currentDiscipline.getName() + "\") - удален!");
                        fillTable();
                    } catch (Exception e) {
                        alertError.showAlert("ОШИБКА УДАЛЕНИЯ ПРЕДМЕТА!", null, e.getMessage());
                        LOGGER.error("handleBtnDeleteDiscipline() ERROR! >>", e);
                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(cellButton);
            }
        }
    }


    public void setEditColumn() {
        Callback<TableColumn, TableCell> cellFactory = new Callback<TableColumn, TableCell>() {
            public TableCell call(TableColumn p) {
                return new EditingCell();
            }
        };

        editNaim.setCellFactory(cellFactory);
        editNaim.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Discipline, String>>() {
            @Override public void handle(TableColumn.CellEditEvent<Discipline, String> t) {

                LOGGER.debug("setEditColumn({})", t);
                try {
                    Discipline editDiscipline = t.getRowValue();
                    editDiscipline.setName(t.getNewValue());
                    disciplineService.updateDiscipline(editDiscipline);
                    alertInfo.showAlert(
                            "ДАННЫЕ ПРЕДМЕТА ОБНОВЛЕНЫ!",
                            null,
                            "НАЗВАНИЕ ПРЕДМЕТА ИЗМЕНЕНО НА: \""+ editDiscipline.getName() + "\"");
                    /*((Faculty)t.getTableView().getItems().get(
                            t.getTablePosition().getRow())).setName(t.getNewValue());*/
                } catch (Exception e) {
                    alertError.showAlert("ОШИБКА ОБНОВЛЕНИЯ ПРЕДМЕТА!", null, e.getMessage());
                    LOGGER.error("setEditColumn() ERROR! >>", e);
                }
            }
        });
    }

    class EditingCell extends TableCell<Discipline, String> {

        private TextField textField;
        public EditingCell() {}

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()*2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }


}
