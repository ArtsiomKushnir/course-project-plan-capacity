package com.kushnir.plancapacity.fxapp.controllers;

import com.kushnir.plancapacity.fxapp.MainClass;
import com.kushnir.plancapacity.fxapp.PopupAlert;
import com.kushnir.plancapacity.model.Department;
import com.kushnir.plancapacity.model.Teacher;
import com.kushnir.plancapacity.service.TeacherService;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.util.Callback;
import javafx.util.StringConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.util.Assert;

import java.io.IOException;

/**
 * Teachers View and Control Class
 */
public class TeachersController {

    private final Logger LOGGER = LogManager.getLogger(TeachersController.class);
    private final PopupAlert alertInfo = new PopupAlert(Alert.AlertType.INFORMATION);
    private final PopupAlert alertError = new PopupAlert(Alert.AlertType.ERROR);
    private TeacherService teacherService =
            (TeacherService) MainClass.applicationContext.getBean("teacherService");

    @FXML private TextField newTeacherName;
    @FXML private ComboBox<Department> comboDepartments;
    @FXML private TableView<Teacher> table;
    @FXML private TableColumn editFIO;
    @FXML private TableColumn action;

    @FXML
    public void initialize() throws Exception {
        LOGGER.debug("initialize()");
        fill();
    }

    public void fill() {
        this.initComboBoxDepartments();
        fillTable();
    }

    public void fillTable() {
        try{
            this.addButtonsToTable();
            this.setEditColumn();
            table.setItems(FXCollections.observableArrayList(this.teacherService.getAllTeachers()));
        } catch (Exception e) {
            LOGGER.error("fillTable() ERROR! >>", e);
        }
    }

    public void initComboBoxDepartments() {
        comboDepartments.setConverter(new StringConverter<Department>() {
            @Override
            public String toString(Department department) {
                return department.getName();
            }
            @Override
            public Department fromString(String string) {
                return null;
            }
        });
        comboDepartments.getItems().removeAll();
        comboDepartments.getItems().addAll(FXCollections.observableArrayList(this.teacherService.getAllDepartments()));
    }

    @FXML
    public void handleBtnAddTeacher(ActionEvent event) throws IOException {
        LOGGER.debug("handleBtnAddTeacher(ActionEvent: {})", event);
        try {
            Assert.hasText(newTeacherName.getText(),"Введите ФИО преподавателя");
            Assert.notNull(comboDepartments.getSelectionModel().getSelectedItem(),"Не выбрана кафедра!");
            Integer id = this.teacherService.addTeacher(new Teacher(newTeacherName.getText(),
                    comboDepartments.getSelectionModel().getSelectedItem().getId()));
            alertInfo.showAlert("ДОБАВЛЕН НОВЫЙ ПРЕПОДАВАТЕЛЬ", null, "ДОБАВЛЕН НОВЫЙ ПРЕПОДАВАТЕЛЬ: "
                    + "ID(" + id +") \""+ newTeacherName.getText() + "\"");
            fillTable();
        } catch (Exception e) {
            alertError.showAlert("ОШИБКА ДОБАВЛЕНИЯ ПРЕПОДАВАТЕЛЯ!", null, e.getMessage());
            LOGGER.error("handleBtnAddTeacher() ERROR! >>", e);
        }
    }

    private void addButtonsToTable() {
        action.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Teacher, Boolean>,
                ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Teacher, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        action.setCellFactory(new Callback<TableColumn<Teacher, Boolean>, TableCell<Teacher, Boolean>>() {
            @Override
            public TableCell<Teacher, Boolean> call(TableColumn<Teacher, Boolean> p) {
                return new ButtonCell();
            }
        });
    }

    private class ButtonCell extends TableCell<Teacher, Boolean> {

        final Button cellButton = new Button("УДАЛИТЬ");

        ButtonCell(){
            cellButton.setOnAction(new EventHandler<ActionEvent>(){
                @Override
                public void handle(ActionEvent t) {
                    LOGGER.debug("handleBtnDeleteTeacher(ActionEvent: {})", t);
                    Teacher currentTeacher =
                            (Teacher) ButtonCell.this.getTableView().getItems().get(ButtonCell.this.getIndex());
                    try {
                        teacherService.deleteTeacher(currentTeacher.getId());
                        alertInfo.showAlert("ПРЕПОДАВАТЕЛЬ УДАЛЕН", null, "ПРЕПОДАВАТЕЛЬ ("
                                +"\""+ currentTeacher.getName() + "\") - удален!");
                        fillTable();
                    } catch (Exception e) {
                        alertError.showAlert("ОШИБКА УДАЛЕНИЯ ПРЕПОДАВАТЕЛЯ!", null, e.getMessage());
                        LOGGER.error("handleBtnDeleteTeacher() ERROR! >>", e);
                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(cellButton);
            }
        }
    }



    public void setEditColumn() {
        Callback<TableColumn, TableCell> cellFactory = new Callback<TableColumn, TableCell>() {
            public TableCell call(TableColumn p) {
                return new EditingCell();
            }
        };

        editFIO.setCellFactory(cellFactory);
        editFIO.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Teacher, String>>() {
            @Override public void handle(TableColumn.CellEditEvent<Teacher, String> t) {

                LOGGER.debug("setEditColumn({})", t);
                try {
                    Teacher editTeacher = t.getRowValue();
                    editTeacher.setName(t.getNewValue());
                    teacherService.updateTeacher(editTeacher);
                    alertInfo.showAlert(
                            "ДАННЫЕ ПРЕПОДАВАТЕЛЯ ОБНОВЛЕНЫ!",
                            null,
                            "ФИО ПРЕПОДАВАТЕЛЯ ИЗМЕНЕНО НА: \""+ editTeacher.getName() + "\"");
                    /*((Faculty)t.getTableView().getItems().get(
                            t.getTablePosition().getRow())).setName(t.getNewValue());*/
                } catch (Exception e) {
                    alertError.showAlert("ОШИБКА ОБНОВЛЕНИЯ ПРЕПОДАВАТЕЛЯ!", null, e.getMessage());
                    LOGGER.error("setEditColumn() ERROR! >>", e);
                }
            }
        });
    }

    class EditingCell extends TableCell<Teacher, String> {

        private TextField textField;
        public EditingCell() {}

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()*2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }


}
