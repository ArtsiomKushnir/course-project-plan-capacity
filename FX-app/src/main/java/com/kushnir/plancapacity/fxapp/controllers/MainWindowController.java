package com.kushnir.plancapacity.fxapp.controllers;

import com.kushnir.plancapacity.fxapp.MainClass;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;

import java.io.IOException;

/**
 * Main view Controller
 */
public class MainWindowController {

    @FXML private Pane pane;
    @FXML private Button btnPlan;
    @FXML private Button btnFaculties;
    @FXML private Button btnDepartments;
    @FXML private Button btnSpecialty;
    @FXML private Button btnTeachers;
    @FXML private Button btnDisciplines;

    @FXML
    public void initialize() throws IOException {
        pane.getChildren().add(FXMLLoader.load(MainClass.class.getClass().getResource("/fxml/plan.fxml")));
    }

    @FXML private void handleBtnPlan(ActionEvent event) throws IOException {
        pane.getChildren().clear();
        pane.getChildren().add(FXMLLoader.load(MainClass.class.getClass().getResource("/fxml/plan.fxml")));
    }
    @FXML private void handleBtnFaculties(ActionEvent event) throws IOException {
        pane.getChildren().clear();
        pane.getChildren().add(FXMLLoader.load(MainClass.class.getClass().getResource("/fxml/faculties.fxml")));
    }
    @FXML private void handleBtnDepartments(ActionEvent event) throws IOException {
        pane.getChildren().clear();
        pane.getChildren().add(FXMLLoader.load(MainClass.class.getClass().getResource("/fxml/departments.fxml")));
    }
    @FXML private void handleBtnSpecialty(ActionEvent event) throws IOException {
        pane.getChildren().clear();
        pane.getChildren().add(FXMLLoader.load(MainClass.class.getClass().getResource("/fxml/specialty.fxml")));
    }
    @FXML private void handleBtnTeachers(ActionEvent event) throws IOException {
        pane.getChildren().clear();
        pane.getChildren().add(FXMLLoader.load(MainClass.class.getClass().getResource("/fxml/teachers.fxml")));
    }
    @FXML private void handleBtnDisciplines(ActionEvent event) throws IOException {
        pane.getChildren().clear();
        pane.getChildren().add(FXMLLoader.load(MainClass.class.getClass().getResource("/fxml/disciplines.fxml")));
    }

}
