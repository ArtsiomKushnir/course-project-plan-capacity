package com.kushnir.plancapacity.fxapp.controllers;

import com.kushnir.plancapacity.fxapp.PopupAlert;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Plane Controller
 */
public class PlanController {

    private final Logger LOGGER = LogManager.getLogger();
    private final PopupAlert alertInfo = new PopupAlert(Alert.AlertType.INFORMATION);
    private final PopupAlert alertError = new PopupAlert(Alert.AlertType.ERROR);

    @FXML
    public void initialize() {

    }
}
