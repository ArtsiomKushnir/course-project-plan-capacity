package com.kushnir.plancapacity.fxapp.controllers;

import com.kushnir.plancapacity.fxapp.PopupAlert;
import com.kushnir.plancapacity.fxapp.MainClass;
import com.kushnir.plancapacity.model.Faculty;
import com.kushnir.plancapacity.service.FacultyService;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Faculties View and Control Class
 */
public class FacultiesController {

    private final Logger LOGGER = LogManager.getLogger();
    private final PopupAlert alertInfo = new PopupAlert(Alert.AlertType.INFORMATION);
    private final PopupAlert alertError = new PopupAlert(Alert.AlertType.ERROR);

    private FacultyService facultyService = (FacultyService) MainClass.applicationContext.getBean("facultyService");

    @FXML private TableView<Faculty> table;
    @FXML private TextField newFacultyName;
    @FXML private TableColumn action;
    @FXML private TableColumn editColumn;

    @FXML
    public void initialize() throws Exception {
        LOGGER.debug("initialize()");
        this.fillTable();
        // this.addRowMouseEvent();
    }

    @FXML
    public void handleBtnAddFaculty(ActionEvent event) throws IOException {
        LOGGER.debug("handleBtnAddFaculty(ActionEvent: {})", event);
        try {
            Integer id = facultyService.addFaculty(new Faculty(newFacultyName.getText()));
            alertInfo.showAlert("ДОБАВЛЕН НОВЫЙ ФАКУЛЬТЕТ", null, "ДОБАВЛЕН НОВЫЙ ФАКУЛЬТЕТ: "
                   + "ID(" + id +") \""+ newFacultyName.getText() + "\"");
            this.fillTable();
        } catch (Exception e) {
            alertError.showAlert("ОШИБКА ДОБАВЛЕНИЯ ФАКУЛЬТЕТА!", null, e.getMessage());
            LOGGER.error("handleBtnAddFaculty() ERROR! >>", e);
        }
    }

    public void fillTable() {
        try{
            this.addButtonsToTable();
            this.setEditColumn();
            table.setItems(FXCollections.observableArrayList(this.facultyService.getAllFaculties()));
        } catch (Exception e) {
            LOGGER.error("fillTable() ERROR! >>", e);
        }
    }

    private void addButtonsToTable() {
        action.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Faculty, Boolean>,
                        ObservableValue<Boolean>>() {
            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Faculty, Boolean> p) {
                return new SimpleBooleanProperty(p.getValue() != null);
            }
        });

        action.setCellFactory(new Callback<TableColumn<Faculty, Boolean>, TableCell<Faculty, Boolean>>() {
            @Override
            public TableCell<Faculty, Boolean> call(TableColumn<Faculty, Boolean> p) {
                return new ButtonCell();
            }
        });
    }

    //Define the button cell
    private class ButtonCell extends TableCell<Faculty, Boolean> {

        final Button cellButton = new Button("УДАЛИТЬ");

        ButtonCell(){
            //Action when the button is pressed
            cellButton.setOnAction(new EventHandler<ActionEvent>(){
                @Override
                public void handle(ActionEvent t) {
                    LOGGER.debug("handleBtnDeleteFaculty(ActionEvent: {})", t);
                    // get Selected Item
                    Faculty currentFaculty =
                            (Faculty) ButtonCell.this.getTableView().getItems().get(ButtonCell.this.getIndex());
                    //remove selected item from the table list
                    try {
                        facultyService.deleteFaculty(currentFaculty.getId());
                        alertInfo.showAlert("ФАКУЛЬТЕТ УДАЛЕН", null, "ФАКУЛЬТЕТ ("
                                +"\""+ currentFaculty.getName() + "\") - удален!");
                        fillTable();
                    } catch (Exception e) {
                        alertError.showAlert("ОШИБКА УДАЛЕНИЯ ФАКУЛЬТЕТА!", null, e.getMessage());
                        LOGGER.error("handleBtnDeleteFaculty() ERROR! >>", e);
                    }
                }
            });
        }

        //Display button if the row is not empty
        @Override
        protected void updateItem(Boolean t, boolean empty) {
            super.updateItem(t, empty);
            if(!empty){
                setGraphic(cellButton);
            }
        }
    }


    /*private void addRowMouseEvent() {
        table.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                    Faculty selectedFaculty = table.getSelectionModel().getSelectedItem();
                    System.out.println(selectedFaculty);
                }
            }
        });
    }*/

    public void setEditColumn() {

        Callback<TableColumn, TableCell> cellFactory = new Callback<TableColumn, TableCell>() {
            public TableCell call(TableColumn p) {
                return new EditingCell();
            }
        };

        editColumn.setCellFactory(cellFactory);
        editColumn.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Faculty, String>>() {
            @Override public void handle(TableColumn.CellEditEvent<Faculty, String> t) {

                LOGGER.debug("setEditColumn({})", t);
                try {
                    Faculty editFaculty = t.getRowValue();
                    editFaculty.setName(t.getNewValue());
                    facultyService.updateFaculty(editFaculty);
                    alertInfo.showAlert(
                            "ФАКУЛЬТЕТ ОБНОВЛЕН!",
                            null,
                            "НАЗВАНИЕ ФАКУЛЬТЕТА ИЗМЕНЕНО НА: \""+ editFaculty.getName() + "\"");
                    /*((Faculty)t.getTableView().getItems().get(
                            t.getTablePosition().getRow())).setName(t.getNewValue());*/
                } catch (Exception e) {
                    alertError.showAlert("ОШИБКА ОБНОВЛЕНИЯ ФАКУЛЬТЕТА!", null, e.getMessage());
                    LOGGER.error("setEditColumn() ERROR! >>", e);
                }
            }
        });
    }

    class EditingCell extends TableCell<Faculty, String> {

        private TextField textField;
        public EditingCell() {}

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap()*2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(textField.getText());
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }
}