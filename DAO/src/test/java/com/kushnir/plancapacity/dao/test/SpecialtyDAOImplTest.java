package com.kushnir.plancapacity.dao.test;

import com.kushnir.plancapacity.dao.SpecialtyDAO;
import com.kushnir.plancapacity.model.Specialty;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.*;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Tests for SpecialtyDAOImpl
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:test-spring-dao-config.xml"})
@Transactional
public class SpecialtyDAOImplTest {

    private static final Logger LOGGER = LogManager.getLogger();

    @Autowired
    SpecialtyDAO specialtyDAO;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        LOGGER.debug("execute: setUpBeforeClass()");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        LOGGER.debug("execute: tearDownAfterClass()");
    }

    @Before
    public void beforeTest() {
        LOGGER.debug("execute: beforeTest()");
    }

    @After
    public void afterTest() {
        LOGGER.debug("execute: afterTest()");
    }

    @Test
    public void getAllSpecialtyTest () {
        List<Specialty> specialtyList = specialtyDAO.getAllSpecialty();
        assertNotNull(specialtyList);
        assertTrue(specialtyList.size() > 0);
        LOGGER.debug("Test: getAllSpecialtyTest(), List<Specialty>: {}", specialtyList);
    }

    @Test
    public void getSpecialtyByFacultyTest () {
        List<Specialty> specialtyList = specialtyDAO.getSpecialtyByFaculty(2);
        assertNotNull(specialtyList);
        assertTrue(specialtyList.size() > 0);
        LOGGER.debug("Test: getSpecialtyByFacultyTest(2), List<Specialty>: {}", specialtyList);
    }

    @Test
    public void getSpecialtyByIdTest () {
        Specialty specialty = specialtyDAO.getSpecialtyById(1);
        assertNotNull(specialty);
        LOGGER.debug("Test: getSpecialtyByIdTest(1), Specialty: {}", specialty);
    }

    @Test
    public void addSpecialtyTest () {
        LOGGER.debug("Test: addSpecialtyTest()");
        Specialty newSpecialty = new Specialty("new Name","10 10-10", 2);
        int countBefore = specialtyDAO.getAllSpecialty().size();
        int newIdSpecialty = specialtyDAO.addSpecialty(newSpecialty);
        assertTrue(newIdSpecialty > 0);
        assertTrue(countBefore < specialtyDAO.getAllSpecialty().size());
    }

    @Test
    public void updateSpecialtyTest () {
        LOGGER.debug("Test: updateSpecialtyTest()");
        Specialty oldSpecialty = specialtyDAO.getSpecialtyById(1);
        LOGGER.debug("Test: Specialty Before: {}", oldSpecialty);
        assertNotNull(oldSpecialty);
        oldSpecialty.setName("Updated name");
        oldSpecialty.setCode("1-1-1");
        specialtyDAO.updateSpecialty(oldSpecialty);
        Specialty updatedSpecialty = specialtyDAO.getSpecialtyById(oldSpecialty.getId());
        LOGGER.debug("Test: Specialty After: {}", updatedSpecialty);
        assertNotNull(updatedSpecialty);
        assertTrue(oldSpecialty.equals(updatedSpecialty));
    }

    @Test
    public void deleteSpecialtyTest () {
        LOGGER.debug("Test: deleteSpecialtyTest()");
        int idSpecialtyForDelete = specialtyDAO.addSpecialty(new Specialty("ForDelete","1",2));
        assertTrue(idSpecialtyForDelete > 0);
        int countBefore = specialtyDAO.getAllSpecialty().size();
        specialtyDAO.deleteSpecialty(idSpecialtyForDelete);
        assertTrue(countBefore > specialtyDAO.getAllSpecialty().size());
    }

    @Test
    public void deleteSpecialtyByFacultyTest () {
        LOGGER.debug("Test: deleteSpecialtyByFacultyTest()");
        int countBefore = specialtyDAO.getSpecialtyByFaculty(2).size();
        assertTrue(countBefore > 0);
        LOGGER.debug("Test: count Before: {}", countBefore);
        specialtyDAO.deleteSpecialtyByFaculty(2);
        int countAfter = specialtyDAO.getSpecialtyByFaculty(2).size();
        LOGGER.debug("Test: count After: {}", countAfter);
        assertTrue(countAfter == 0);
    }

}
