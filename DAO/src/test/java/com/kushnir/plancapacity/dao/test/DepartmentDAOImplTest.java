package com.kushnir.plancapacity.dao.test;

import com.kushnir.plancapacity.dao.DepartmentDAO;
import com.kushnir.plancapacity.model.Department;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.*;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Department DAO implementation Tests
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:test-spring-dao-config.xml"})
@Transactional
public class DepartmentDAOImplTest {

    private static final Logger LOGGER = LogManager.getLogger();

    @Autowired
    DepartmentDAO departmentDAO;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        LOGGER.debug("execute: setUpBeforeClass()");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        LOGGER.debug("execute: tearDownAfterClass()");
    }

    @Before
    public void beforeTest() {
        LOGGER.debug("execute: beforeTest()");
    }

    @After
    public void afterTest() {
        LOGGER.debug("execute: afterTest()");
    }

    @Test
    public void getAllDepartmentsTest () {
        List<Department> departmentList = departmentDAO.getAllDepartments();
        assertNotNull(departmentList);
        assertTrue(departmentList.size() > 0);
        LOGGER.debug("Test: getAllDepartmentsTest(), List<Department>: {}", departmentList);
    }

    @Test
    public void getAllDepartmentsByFacultyTest () {
        List<Department> departmentList = departmentDAO.getAllDepartmentsByFaculty(2);
        assertNotNull(departmentList);
        assertTrue(departmentList.size() > 0);
        LOGGER.debug("Test: getAllDepartmentsByFacultyTest(2), List<Department>: {}", departmentList);
    }

    @Test
    public void getDepartmentByIdTest () {
        Department department = departmentDAO.getDepartmentById(1);
        assertNotNull(department);
        LOGGER.debug("Test: getDepartmentByIdTest(1), Department: {}", department);
    }

    @Test
    public void addDepartmentTest () {
        LOGGER.debug("Test: addDepartmentTest()");
        Department newDepartment = new Department("New Name", 2);
        int countBefore = departmentDAO.getAllDepartments().size();
        int idAddedDepartment = departmentDAO.addDepartment(newDepartment);
        assertNotNull(idAddedDepartment);
        assertTrue(idAddedDepartment > 0);
        assertTrue(countBefore < departmentDAO.getAllDepartments().size());
    }

    @Test
    public void updateDepartmentTest () {
        LOGGER.debug("Test: updateDepartmentTest()");
        Department oldDepartment = departmentDAO.getDepartmentById(1);
        oldDepartment.setName("Updated Name");
        departmentDAO.updateDepartment(oldDepartment);
        assertTrue(oldDepartment.equals(departmentDAO.getDepartmentById(oldDepartment.getId())));
    }

    @Test
    public void deleteDepartmentTest () {
        LOGGER.debug("Test: deleteDepartmentTest()");
        int addedNewDepartment = departmentDAO.addDepartment(new Department());
        assertTrue(addedNewDepartment > 0);
        int countBefore = departmentDAO.getAllDepartments().size();
        departmentDAO.deleteDepartment(addedNewDepartment);
        assertTrue(countBefore > departmentDAO.getAllDepartments().size());

        try {  assertNull(departmentDAO.getDepartmentById(addedNewDepartment)); }
        catch (EmptyResultDataAccessException e) {   }
    }

    @Test
    public void deleteDepartmentByFacultyTest () {
        LOGGER.debug("Test: deleteDepartmentByFacultyTest()");
        int countBefore = departmentDAO.getAllDepartmentsByFaculty(2).size();
        departmentDAO.deleteDepartmentByFaculty(2);
        int countAfter = departmentDAO.getAllDepartmentsByFaculty(2).size();
        assertTrue(countAfter == 0);
        assertTrue(countBefore > countAfter);
    }
}