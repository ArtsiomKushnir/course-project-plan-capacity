package com.kushnir.plancapacity.dao;

import com.kushnir.plancapacity.model.Discipline;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Discipline DAO implementation
 */
public class DisciplineDAOImpl implements DisciplineDAO {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final String P_ID = "p_id";
    private final String P_NAME = "p_name";
    private final String P_ID_DEPARTMENT = "p_id_department";

    @Value("${discipline.getAll}")
    private String getAllSqlQuery;

    @Value("${discipline.getByDepartment}")
    private String getByDepartmentSqlQuery;

    @Value("${discipline.getById}")
    private String getByIdSqlQuery;

    @Value("${discipline.add}")
    private String addSqlQuery;

    @Value("${discipline.update}")
    private String updateSqlQuery;

    @Value("${discipline.delete}")
    private String deleteSqlQuery;

    @Value("${discipline.deleteByDepartment}")
    private String deleteByDepartmentSqlQuery;

    @Override
    public List<Discipline> getAllDisciplines() throws DataAccessException {
        return namedParameterJdbcTemplate.query(getAllSqlQuery, new DisciplineRowMapper());
    }

    @Override
    public List<Discipline> getAllDisciplinesByDepartment(int idDepartment) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource(P_ID_DEPARTMENT, idDepartment);
        return namedParameterJdbcTemplate.query(
                getByDepartmentSqlQuery,
                parameterSource,
                new DisciplineRowMapper()
        );
    }

    @Override
    public Discipline getDisciplineById(int idDiscipline) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource(P_ID, idDiscipline);
        return namedParameterJdbcTemplate.queryForObject(
                getByIdSqlQuery,
                parameterSource,
                new DisciplineRowMapper()
        );
    }

    @Override
    public Integer addDiscipline(Discipline discipline) throws DataAccessException {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_NAME, discipline.getName());
        parameterSource.addValue(P_ID_DEPARTMENT, discipline.getIdDepartment());

        namedParameterJdbcTemplate.update(addSqlQuery, parameterSource, keyHolder);

        return keyHolder.getKey().intValue();
    }

    @Override
    public void updateDiscipline(Discipline discipline) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_NAME, discipline.getName());
        parameterSource.addValue(P_ID_DEPARTMENT, discipline.getIdDepartment());
        parameterSource.addValue(P_ID, discipline.getId());

        namedParameterJdbcTemplate.update(updateSqlQuery, parameterSource);
    }

    @Override
    public void deleteDiscipline(int idDiscipline) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_ID, idDiscipline);

        namedParameterJdbcTemplate.update(deleteSqlQuery, parameterSource);
    }

    @Override
    public void deleteDisciplineByDepartment(int idDepartment) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_ID_DEPARTMENT, idDepartment);

        namedParameterJdbcTemplate.update(deleteByDepartmentSqlQuery, parameterSource);
    }

    private class DisciplineRowMapper implements RowMapper<Discipline> {

        @Override
        public Discipline mapRow(ResultSet resultSet, int i) throws SQLException {
            Discipline discipline = new Discipline(
                    resultSet.getInt("id_discipline")
                    , resultSet.getString("name_discipline")
                    , resultSet.getInt("id_department")
                    , resultSet.getString("name_department"));
            return discipline;
        }
    }
}
