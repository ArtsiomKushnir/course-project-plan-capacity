package com.kushnir.plancapacity.dao;

import com.kushnir.plancapacity.model.Faculty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Faculty DAO implementation
 */
public class FacultyDAOImpl implements FacultyDAO {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final String P_ID = "p_id";
    private final String P_NAME = "p_name";

    @Value("${faculty.getAll}")
    private String getAllSqlQuery;

    @Value("${faculty.getById}")
    private String getByIdSqlQuery;

    @Value("${faculty.getByName}")
    private String getByNameSqlQuery;

    @Value("${faculty.add}")
    private String addSqlQuery;

    @Value("${faculty.update}")
    private String updateSqlQuery;

    @Value("${faculty.delete}")
    private String deleteSqlQuery;

    @Override
    public List<Faculty> getAllFaculties() throws DataAccessException {
        return namedParameterJdbcTemplate.query(getAllSqlQuery, new FacultyRowMapper());
    }

    @Override
    public Faculty getFaculty(int idFaculty) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource(P_ID, idFaculty);
        return namedParameterJdbcTemplate.queryForObject(
                getByIdSqlQuery,
                parameterSource,
                new FacultyRowMapper()
        );
    }

    @Override
    public Faculty getFacultyByName(String name) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource(P_NAME, name);
        return namedParameterJdbcTemplate.queryForObject(
                getByNameSqlQuery,
                parameterSource,
                new FacultyRowMapper()
        );
    }

    @Override
    public Integer addFaculty(Faculty faculty) throws DataAccessException {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameterSource = new MapSqlParameterSource(P_NAME, faculty.getName());

        namedParameterJdbcTemplate.update(addSqlQuery, parameterSource, keyHolder);

        return keyHolder.getKey().intValue();
    }

    @Override
    public void updateFaculty(Faculty faculty) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_NAME, faculty.getName());
        parameterSource.addValue(P_ID, faculty.getId());

        namedParameterJdbcTemplate.update(updateSqlQuery, parameterSource);
    }

    @Override
    public void deleteFaculty(int idFaculty) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_ID, idFaculty);

        namedParameterJdbcTemplate.update(deleteSqlQuery, parameterSource);
    }

    private class FacultyRowMapper implements RowMapper<Faculty> {

        @Override
        public Faculty mapRow(ResultSet resultSet, int i) throws SQLException {
            Faculty faculty = new Faculty(
                    resultSet.getInt("id_faculty")
                    , resultSet.getString("name_faculty"));
            return faculty;
        }
    }
}
