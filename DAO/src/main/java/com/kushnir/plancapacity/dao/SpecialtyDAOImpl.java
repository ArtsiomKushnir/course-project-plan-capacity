package com.kushnir.plancapacity.dao;

import com.kushnir.plancapacity.model.Specialty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Specialty DAO implementation
 */
public class SpecialtyDAOImpl implements SpecialtyDAO {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final String P_ID = "p_id";
    private final String P_NAME = "p_name";
    private final String P_CODE = "p_code";
    private final String P_ID_FACULTY = "p_id_faculty";

    @Value("${specialty.getAll}")
    private String getAllSpecialtySqlQuery;

    @Value("${specialty.getByFaculty}")
    private String getSpecialtyByFacultySqlQuery;

    @Value("${specialty.getById}")
    private String getSpecialtyByIdSqlQuery;

    @Value("${specialty.add}")
    private String addSpecialtySqlQuery;

    @Value("${specialty.update}")
    private String updateSpecialtySqlQuery;

    @Value("${specialty.delete}")
    private String deleteSpecialtySqlQuery;

    @Value("${specialty.deleteByFaculty}")
    private String deleteSpecialtyByFacultySqlQuery;

    @Override
    public List<Specialty> getAllSpecialty() throws DataAccessException {
        return namedParameterJdbcTemplate.query(getAllSpecialtySqlQuery, new SpecialtyRowMapper());
    }

    @Override
    public List<Specialty> getSpecialtyByFaculty(int facultyId) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource(P_ID_FACULTY, facultyId);
        return namedParameterJdbcTemplate.query(
                getSpecialtyByFacultySqlQuery
                , parameterSource
                , new SpecialtyRowMapper());
    }

    @Override
    public Specialty getSpecialtyById (int id) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource(P_ID, id);
        return namedParameterJdbcTemplate.queryForObject(
                getSpecialtyByIdSqlQuery
                , parameterSource
                , new SpecialtyRowMapper());
    }

    @Override
    public Integer addSpecialty(Specialty newSpecialty) throws DataAccessException {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_NAME, newSpecialty.getName());
        parameterSource.addValue(P_CODE, newSpecialty.getCode());
        parameterSource.addValue(P_ID_FACULTY, newSpecialty.getId_faculty());

        namedParameterJdbcTemplate.update(addSpecialtySqlQuery, parameterSource, keyHolder);

        return keyHolder.getKey().intValue();
    }

    @Override
    public void updateSpecialty(Specialty specialty) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_ID, specialty.getId());
        parameterSource.addValue(P_NAME, specialty.getName());
        parameterSource.addValue(P_CODE, specialty.getCode());
        parameterSource.addValue(P_ID_FACULTY, specialty.getId_faculty());

        namedParameterJdbcTemplate.update(updateSpecialtySqlQuery, parameterSource);
    }

    @Override
    public void deleteSpecialty(int specialtyId) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_ID, specialtyId);

        namedParameterJdbcTemplate.update(deleteSpecialtySqlQuery, parameterSource);
    }

    @Override
    public void deleteSpecialtyByFaculty(int facultyId) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_ID_FACULTY, facultyId);

        namedParameterJdbcTemplate.update(deleteSpecialtyByFacultySqlQuery, parameterSource);
    }

    private class SpecialtyRowMapper implements RowMapper<Specialty> {

        @Override
        public Specialty mapRow(ResultSet resultSet, int i) throws SQLException {
            Specialty specialty = new Specialty(
                    resultSet.getInt("id_speciality")
                    , resultSet.getString("name_speciality")
                    , resultSet.getString("code_speciality")
                    , resultSet.getInt("id_faculty")
                    , resultSet.getString("name_faculty"));
            return specialty;
        }
    }
}
