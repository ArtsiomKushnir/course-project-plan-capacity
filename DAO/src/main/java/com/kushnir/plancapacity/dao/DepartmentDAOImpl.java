package com.kushnir.plancapacity.dao;

import com.kushnir.plancapacity.model.Department;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Department DAO implementation
 */
public class DepartmentDAOImpl implements DepartmentDAO {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private String P_ID = "p_id";
    private String P_NAME = "p_name";
    private String P_ID_FACULTY = "p_id_faculty";

    @Value("${department.getAll}")
    private String getAllDepartmentsSqlQuery;

    @Value("${department.getByFaculty}")
    private String getDepartmentsByFacultySqlQuery;

    @Value("${department.getById}")
    private String getDepartmentSqlQuery;

    @Value("${department.add}")
    private String addDepartmentSqlQuery;

    @Value("${department.update}")
    private String updateDepartmentSqlQuery;

    @Value("${department.delete}")
    private String deleteDepartmentSqlQuery;

    @Value("${department.deleteByFaculty}")
    private String deleteDepartmentsByFacultySqlQuery;

    @Override
    public List<Department> getAllDepartments() throws DataAccessException {
        return namedParameterJdbcTemplate.query(getAllDepartmentsSqlQuery,  new DepartmentRowMapper());
    }

    @Override
    public List<Department> getAllDepartmentsByFaculty(int facultyId) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource(P_ID_FACULTY, facultyId);
        return namedParameterJdbcTemplate.query(getDepartmentsByFacultySqlQuery
                , parameterSource, new DepartmentRowMapper());
    }

    @Override
    public Department getDepartmentById(int id) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource(P_ID, id);
        return namedParameterJdbcTemplate.queryForObject(getDepartmentSqlQuery
                , parameterSource, new DepartmentRowMapper());
    }

    @Override
    public Integer addDepartment(Department department) throws DataAccessException {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_NAME, department.getName());
        parameterSource.addValue(P_ID_FACULTY, department.getId_faculty());

        namedParameterJdbcTemplate.update(addDepartmentSqlQuery, parameterSource, keyHolder);

        return keyHolder.getKey().intValue();
    }

    @Override
    public void updateDepartment(Department department) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_ID, department.getId());
        parameterSource.addValue(P_NAME, department.getName());
        parameterSource.addValue(P_ID_FACULTY, department.getId_faculty());

        namedParameterJdbcTemplate.update(updateDepartmentSqlQuery, parameterSource);
    }

    @Override
    public void deleteDepartment(int id) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource(P_ID, id);
        namedParameterJdbcTemplate.update(deleteDepartmentSqlQuery, parameterSource);
    }

    @Override
    public void deleteDepartmentByFaculty(int facultyId) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource(P_ID_FACULTY, facultyId);
        namedParameterJdbcTemplate.update(deleteDepartmentsByFacultySqlQuery, parameterSource);
    }

    private class DepartmentRowMapper implements RowMapper<Department> {

        @Override
        public Department mapRow(ResultSet resultSet, int i) throws SQLException {
            Department department = new Department(
                    resultSet.getInt("id_department")
                    , resultSet.getString("name_department")
                    , resultSet.getInt("id_faculty")
                    , resultSet.getString("name_faculty"));
            return department;
        }
    }
}
