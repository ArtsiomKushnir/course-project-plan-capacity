package com.kushnir.plancapacity.dao;

import com.kushnir.plancapacity.model.Teacher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Teacher DAO implementation
 */
public class TeacherDAOImpl implements TeacherDAO {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final String P_ID = "p_id";
    private final String P_NAME = "p_name";
    private final String P_ID_DEPARTMENT = "p_id_department";

    @Value("${teacher.getAll}")
    private String getAllSqlQuery;

    @Value("${teacher.getByDepartment}")
    private String getByDepartmentSqlQuery;

    @Value("${teacher.getById}")
    private String getByIdSqlQuery;

    @Value("${teacher.add}")
    private String addSqlQuery;

    @Value("${teacher.update}")
    private String updateSqlQuery;

    @Value("${teacher.delete}")
    private String deleteSqlQuery;

    @Value("${teacher.deleteByDepartment}")
    private String deleteByDepartmentSqlQuery;

    @Override
    public List<Teacher> getAllTeachers() throws DataAccessException {
        return namedParameterJdbcTemplate.query(getAllSqlQuery, new TeacherRowMapper());
    }

    @Override
    public List<Teacher> getAllTeachersByDepartment(Integer idDepartment) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource(P_ID_DEPARTMENT, idDepartment);
        return namedParameterJdbcTemplate.query(
                getByDepartmentSqlQuery,
                parameterSource,
                new TeacherRowMapper()
        );
    }

    @Override
    public Teacher getTeacherById(Integer idTeacher) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource(P_ID, idTeacher);
        return namedParameterJdbcTemplate.queryForObject(
                getByIdSqlQuery,
                parameterSource,
                new TeacherRowMapper()
        );
    }

    @Override
    public Integer addTeacher(Teacher teacher) throws DataAccessException {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_NAME, teacher.getName());
        parameterSource.addValue(P_ID_DEPARTMENT, teacher.getDepartment());

        namedParameterJdbcTemplate.update(addSqlQuery, parameterSource, keyHolder);

        return keyHolder.getKey().intValue();
    }

    @Override
    public void updateTeacher(Teacher teacher) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_NAME, teacher.getName());
        parameterSource.addValue(P_ID_DEPARTMENT, teacher.getDepartment());
        parameterSource.addValue(P_ID, teacher.getId());

        namedParameterJdbcTemplate.update(updateSqlQuery, parameterSource);
    }

    @Override
    public void deleteTeacher(Integer idTeacher) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_ID, idTeacher);

        namedParameterJdbcTemplate.update(deleteSqlQuery, parameterSource);
    }

    @Override
    public void deleteTeachersByDepartment(Integer idDepartment) throws DataAccessException {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue(P_ID_DEPARTMENT, idDepartment);

        namedParameterJdbcTemplate.update(deleteByDepartmentSqlQuery, parameterSource);
    }

    private class TeacherRowMapper implements RowMapper<Teacher> {

        @Override
        public Teacher mapRow(ResultSet resultSet, int i) throws SQLException {
            Teacher teacher = new Teacher(
                    resultSet.getInt("id_teacher")
                    , resultSet.getString("name_teacher")
                    , resultSet.getInt("id_department")
                    , resultSet.getString("name_department"));
            return teacher;
        }
    }
}
