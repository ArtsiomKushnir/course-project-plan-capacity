package com.kushnir.plancapacity.dao;

import com.kushnir.plancapacity.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * User DAO implementation
 */

public class UserDAOImpl implements UserDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Value("${user.getAll}")
    String getAllUsersSqlQuery;

    @Override
    public List<User> getAllUsers() throws DataAccessException {
        return jdbcTemplate.query(getAllUsersSqlQuery, new UserRowMapper());
    }

    private class UserRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet resultSet, int i) throws SQLException {
            User user = new User(
                    resultSet.getInt("id_user"),
                    resultSet.getString("login_user"),
                    resultSet.getString("name_user"),
                    resultSet.getString("password_user"));
            return user;
        }
    }
}
