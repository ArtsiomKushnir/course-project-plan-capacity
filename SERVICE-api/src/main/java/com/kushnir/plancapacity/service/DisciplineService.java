package com.kushnir.plancapacity.service;

import com.kushnir.plancapacity.model.Department;
import com.kushnir.plancapacity.model.Discipline;
import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * Discipline Service API.
 */
public interface DisciplineService {

    List<Discipline> getAllDisciplines () throws DataAccessException;
    List<Discipline> getAllDisciplinesByDepartment(int idDepartment) throws DataAccessException;
    List<Department> getAllDepartments() throws DataAccessException;
    Discipline getDisciplineById (int idDiscipline) throws DataAccessException;
    Integer addDiscipline (Discipline discipline) throws DataAccessException;
    void updateDiscipline (Discipline discipline) throws DataAccessException;
    void deleteDiscipline (int idDiscipline) throws DataAccessException;
    void deleteDisciplineByDepartment(int idDepartment) throws DataAccessException;

}
