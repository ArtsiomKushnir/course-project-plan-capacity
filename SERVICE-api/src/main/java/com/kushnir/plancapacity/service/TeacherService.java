package com.kushnir.plancapacity.service;

import com.kushnir.plancapacity.model.Department;
import com.kushnir.plancapacity.model.Teacher;

import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * TeacherService interface
 */
public interface TeacherService {

    List<Teacher> getAllTeachers() throws DataAccessException;
    List<Teacher> getAllTeachersByDepartment (int idDepartment) throws DataAccessException;
    List<Department> getAllDepartments() throws DataAccessException;
    Teacher getTeacherById (int idTeacher) throws DataAccessException;
    Integer addTeacher (Teacher teacher) throws DataAccessException;
    void updateTeacher (Teacher teacher) throws DataAccessException;
    void deleteTeacher (int idTeacher) throws DataAccessException;
    void deleteTeachersByDepartment (int idDepartment) throws DataAccessException;
}
