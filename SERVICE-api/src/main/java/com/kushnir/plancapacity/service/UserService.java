package com.kushnir.plancapacity.service;

import com.kushnir.plancapacity.model.User;

import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * User Service intrface
 */
public interface UserService {

    List<User> getAllUsers() throws DataAccessException;
}
