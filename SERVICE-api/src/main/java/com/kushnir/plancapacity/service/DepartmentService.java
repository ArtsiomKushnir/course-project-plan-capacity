package com.kushnir.plancapacity.service;

import com.kushnir.plancapacity.model.Department;
import com.kushnir.plancapacity.model.Faculty;
import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * Department Service API
 */
public interface DepartmentService {

    List<Department> getAllDepartments () throws DataAccessException;
    List<Department> getAllDepartmentsByFaculty (int facultyId) throws DataAccessException;
    List<Faculty> getAllFaculties() throws DataAccessException;
    Department getDepartmentById (int id) throws DataAccessException;
    Integer addDepartment (Department department) throws DataAccessException;
    void updateDepartment (Department department) throws DataAccessException;
    void deleteDepartment (int id) throws DataAccessException;
    void deleteDepartmentByFaculty (int facultyId) throws DataAccessException;
}
