package com.kushnir.plancapacity.service;

import com.kushnir.plancapacity.model.Faculty;
import com.kushnir.plancapacity.model.Specialty;
import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * SpecialtyService interface
 */
public interface SpecialtyService {

    List<Specialty> getAllSpecialty() throws DataAccessException;
    List<Specialty> getAllSpecialtyByFaculty (int facultyId) throws DataAccessException;
    List<Faculty> getAllFaculties() throws DataAccessException;
    Specialty getSpecialtyById (int id) throws DataAccessException;
    Integer addSpecialty (Specialty specialty) throws DataAccessException;
    void updateSpecialty (Specialty specialty) throws DataAccessException;
    void deleteSpecialty (int id) throws DataAccessException;
    void deleteSpecialtyByFaculty (int facultyId) throws DataAccessException;
}
