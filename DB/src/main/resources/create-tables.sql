DROP TABLE IF EXISTS users;
CREATE TABLE users (
    id_user          INT             NOT NULL AUTO_INCREMENT,
    login_user       VARCHAR(100)    NOT NULL UNIQUE,
    name_user        VARCHAR(150)    NOT NULL,
    password_user    VARCHAR(8)      NOT NULL,
    PRIMARY KEY (id_user),
    UNIQUE KEY `login_user` (login_user)
);

DROP TABLE IF EXISTS rights;
CREATE TABLE rights (
    id_right         INT             NOT NULL AUTO_INCREMENT,
    name_right       VARCHAR(50)     NOT NULL,
    PRIMARY KEY (id_right),
    UNIQUE KEY `name_right` (name_right)
);

DROP TABLE IF EXISTS users_rights;
CREATE TABLE users_rights (
	id				 INT			 NOT NULL AUTO_INCREMENT,
    id_user          INT             NOT NULL,
    id_right         INT             NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id_user)            REFERENCES users(id_user),
    FOREIGN KEY (id_right)           REFERENCES rights(id_right),
    UNIQUE KEY `users_rights` (id_user, id_right)
);

-- =========================================================================

DROP TABLE IF EXISTS faculties;
CREATE TABLE faculties (
    id_faculty       INT             NOT NULL AUTO_INCREMENT,
    name_faculty     VARCHAR(200)    NOT NULL,
    PRIMARY KEY (id_faculty),
    UNIQUE KEY `name_faculty` (name_faculty)
);

DROP TABLE IF EXISTS departments;
CREATE TABLE departments (
    id_department    INT             NOT NULL AUTO_INCREMENT,
    name_department  VARCHAR(200)    NOT NULL,
    id_faculty       INT             NOT NULL,
    PRIMARY KEY (id_department),
    FOREIGN KEY (id_faculty)         REFERENCES faculties(id_faculty),
    UNIQUE KEY `department_faculty` (name_department, id_faculty),
    UNIQUE KEY `department` (name_department)
);

DROP TABLE IF EXISTS specialities;
CREATE TABLE specialities (
    id_speciality    INT             NOT NULL AUTO_INCREMENT,
    name_speciality  VARCHAR(200)    NOT NULL,
    code_speciality  VARCHAR(12)     NOT NULL UNIQUE,
    id_faculty       INT             NOT NULL,
    PRIMARY KEY (id_speciality),
    FOREIGN KEY (id_faculty)         REFERENCES faculties(id_faculty),
    UNIQUE KEY `speciality_code_faculty` (name_speciality, code_speciality, id_faculty)
);

DROP TABLE IF EXISTS disciplines;
CREATE TABLE disciplines (
    id_discipline    INT             NOT NULL AUTO_INCREMENT,
    name_discipline  VARCHAR(200)    NOT NULL,
    id_department    INT             NOT NULL,
    PRIMARY KEY (id_discipline),
    FOREIGN KEY (id_department)      REFERENCES departments(id_department),
    UNIQUE KEY `discipline_department` (name_discipline, id_department)
);

DROP TABLE IF EXISTS disc_types;
CREATE TABLE disc_types (
    id_type          INT             NOT NULL AUTO_INCREMENT,
    name_type        VARCHAR(200)    NOT NULL,
    PRIMARY KEY (id_type),
    UNIQUE KEY `name_type` (name_type)
);

DROP TABLE IF EXISTS teachers;
CREATE TABLE teachers (
    id_teacher       INT             NOT NULL AUTO_INCREMENT,
    name_teacher     VARCHAR(200)    NOT NULL,
    id_department    INT             NOT NULL,
    PRIMARY KEY (id_teacher),
    FOREIGN KEY (id_department)      REFERENCES departments(id_department),
    UNIQUE KEY `teacher_department` (name_teacher, id_department),
    UNIQUE KEY `teacher` (name_teacher)
);

DROP TABLE IF EXISTS capacity;
CREATE TABLE capacity (
	  id_capacity		   INT			       NOT NULL AUTO_INCREMENT,
    semestr          BOOLEAN         NOT NULL, -- ENUM('SPRING', 'AUTUMN') NOT NULL,
    academic_year    YEAR            NOT NULL,
    id_speciality    INT             NOT NULL,
    id_teacher       INT             NOT NULL,
    id_discipline    INT             NOT NULL,
    id_type          INT             NOT NULL,
    count_hours      REAL            NOT NULL,
    PRIMARY KEY (id_capacity),
    FOREIGN KEY (id_speciality)      REFERENCES specialities(id_speciality),
    FOREIGN KEY (id_teacher)         REFERENCES teachers(id_teacher),
    FOREIGN KEY (id_type)            REFERENCES disc_types(id_type),
    UNIQUE KEY `capacityUKey1` (academic_year, semestr, id_speciality, id_teacher, id_discipline, id_type)
);