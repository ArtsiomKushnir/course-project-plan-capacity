package com.kushnir.plancapacity.dao;

import com.kushnir.plancapacity.model.Specialty;
import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * Specialty DAO interface
 */
public interface SpecialtyDAO {

    List<Specialty> getAllSpecialty () throws DataAccessException;
    List<Specialty> getSpecialtyByFaculty (int facultyId) throws DataAccessException;
    Specialty getSpecialtyById (int id) throws DataAccessException;
    Integer addSpecialty (Specialty newSpecialty) throws DataAccessException;
    void updateSpecialty (Specialty specialty) throws DataAccessException;
    void deleteSpecialty (int specialtyId) throws DataAccessException;
    void deleteSpecialtyByFaculty (int facultyId) throws DataAccessException;
}