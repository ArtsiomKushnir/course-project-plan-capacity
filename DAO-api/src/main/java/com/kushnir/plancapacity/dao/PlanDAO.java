package com.kushnir.plancapacity.dao;

import com.kushnir.plancapacity.model.Capacity;

import java.util.ArrayList;

public interface PlanDAO {

    ArrayList<Plan> searchPlans(Boolean semestr,
                                       String academicYear,
                                       Integer idSpecialty,
                                       Integer idTeacher,
                                       Integer idDiscipline,
                                       Integer idType);

    ArrayList<Capacity> searchCapacity(Boolean semestr,
                                       String academicYear,
                                       Integer idSpecialty,
                                       Integer idTeacher,
                                       Integer idDiscipline,
                                       Integer idType);
    ArrayList<Capacity> getCapacityByTeacherId(Integer idTeacher);
    Capacity getCapacityById(Integer idCapacity);
    void updateCapacity(Capacity capacity);
    Integer addCapacity(Capacity capacity);
    void deleteCapacityById(Integer idCapacity);
}