package com.kushnir.plancapacity.dao;

import com.kushnir.plancapacity.model.Teacher;
import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * Teacher interface
 */
public interface TeacherDAO {

    List<Teacher> getAllTeachers () throws DataAccessException;
    List<Teacher> getAllTeachersByDepartment (Integer idDepartment) throws DataAccessException;
    Teacher getTeacherById (Integer idTeacher) throws DataAccessException;
    Integer addTeacher (Teacher teacher) throws DataAccessException;
    void updateTeacher (Teacher teacher) throws DataAccessException;
    void deleteTeacher (Integer idTeacher) throws DataAccessException;
    void deleteTeachersByDepartment (Integer idDepartment) throws DataAccessException;
}
