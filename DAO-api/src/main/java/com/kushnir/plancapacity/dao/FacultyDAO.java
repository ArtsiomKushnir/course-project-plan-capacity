package com.kushnir.plancapacity.dao;

import com.kushnir.plancapacity.model.Faculty;

import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * Faculty DAO interface
 */
public interface FacultyDAO {

    List<Faculty> getAllFaculties() throws DataAccessException;
    Faculty getFaculty (int id) throws DataAccessException;
    Faculty getFacultyByName (String name) throws DataAccessException;
    Integer addFaculty (Faculty faculty) throws DataAccessException;
    void updateFaculty (Faculty faculty) throws DataAccessException;
    void deleteFaculty (int id) throws DataAccessException;
}
