package com.kushnir.plancapacity.dao;

import com.kushnir.plancapacity.model.Discipline;

import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * Discipline DAO interface
 */
public interface DisciplineDAO {

    List<Discipline> getAllDisciplines () throws DataAccessException;
    List<Discipline> getAllDisciplinesByDepartment(int idDepartment) throws DataAccessException;
    Discipline getDisciplineById (int idDiscipline) throws DataAccessException;
    Integer addDiscipline (Discipline discipline) throws DataAccessException;
    void updateDiscipline (Discipline discipline) throws DataAccessException;
    void deleteDiscipline (int idDiscipline) throws DataAccessException;
    void deleteDisciplineByDepartment(int idDepartment) throws DataAccessException;
}
