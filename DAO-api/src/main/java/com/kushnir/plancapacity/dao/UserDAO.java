package com.kushnir.plancapacity.dao;

import com.kushnir.plancapacity.model.User;

import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * TODO edit
 */
public interface UserDAO {

    List<User> getAllUsers () throws DataAccessException;
}
