package com.kushnir.plancapacity.dao;

import com.kushnir.plancapacity.model.Department;

import org.springframework.dao.DataAccessException;

import java.util.List;

/**
 * Department DAO interface
 */
public interface DepartmentDAO {

    List<Department> getAllDepartments () throws DataAccessException;
    List<Department> getAllDepartmentsByFaculty (int facultyId) throws DataAccessException;
    Department getDepartmentById (int id) throws DataAccessException;
    Integer addDepartment (Department department) throws DataAccessException;
    void updateDepartment (Department department) throws DataAccessException;
    void deleteDepartment (int id) throws DataAccessException;
    void deleteDepartmentByFaculty (int facultyId) throws DataAccessException;
}
